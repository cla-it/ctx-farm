##
# resource "<type>" (
#                  "vmware_vcenter";
#                  "vmware_esxi_host";
#                  "hyper-v";
#                  "kvm";
#                  "cloustack";
#                  "win_host";
#                  "lnx_host";
#                  "xen"
# )
resource = "vmware_vcenter" "CTXFARM" {
    hostname = "vcenter.it.net"
    ip       = "151.2.15.9"
    url      = "https://vcenter.it.net"

    group = "resource-group" "CLA" {
      project  = "17000000-19"
      login    = "bv003957"
      password = "izAnAgi77?perM%sUfi"
      label    =  Accesso vCenter Supernap CLA

        resource = "resource-pool" "bv003957_develop(nobck)" {
            # resource "<type>" (
            #               "resource-pool";"vApps";"vHost"
            # ) 
            #  
            # resource name "<name-alias-rsc>" ( 
            ##    resource = "xxx"
            ##    hostname = "xxx"
            ##    ip       = "xxx"
            ##    login    = "xxx"
            ##    password = "xxx"
            ##    label    =  xxx
            ##    web_urkl =  xxx
            ##    os       = "xxx"
            ##    roles    = "xxx"
            ##    features = "xxx"
            ##    software = (
            ##
            ##      resource    "xxx"
            ##      login       "xxx"
            ##      password    "xxx"
            ##      serverdb    "xxx"
            ##      database    "xxx"
            ##      usr_db      "xxx"
            ##      pwd_db      "xxx"
            ## )

            resource = "vApps" "cl2001" {

                resource = "vHost" "clsrv01" 
                hostname = "clsrv001.ctxfarm.com"
                ip       = "192.168.63.10"
                login    =  
                password =
                label    =
            }
        }

        resource = "resource-pool" "bv003957_work(bck30)" {
            # resource "<type>" (
            #               "resource-pool";"vApps";"vHost"
            # ) 
            #  
            # resource name "<name-alias-rsc>" ( 
            ##    resource = "xxx"
            ##    hostname = "xxx"
            ##    ip       = "xxx"
            ##    login    = "xxx"
            ##    password = "xxx"
            ##    label    =  xxx
            ##    web_urkl =  xxx
            ##    os       = "xxx"
            ##    roles    = "xxx"
            ##    features = "xxx"
            ##    software = ( 
            ##                  ftp;
            ##                  mail;
            ##                  ctx;
            ##                  bck;
            ##
            ##      resource    "type" "name"
            ##      login       "xxx"
            ##      password    "xxx"
            ##      serverdb    "xxx"
            ##      database    "xxx"
            ##      usr_db      "xxx"
            ##      pwd_db      "xxx"
            ## )

            resource = "vApps" "@@DMZ_claweb" {

                resource = "vHost" "claweb001" 
                hostname = "claweb001.ctxfarm.com"
                ip       = "10.10.10.201"
                login    = 
                password =
                label    =  old web

                resource = "vHost" "claweb002" 
                hostname = "claweb002.ctxfarm.com"
                ip       = "10.10.10.201-11"
                login    = 
                password =
                label    =  CLA app

                resource = "vHost" "claweb003"
                hostname = "claweb003.ctxfarm.com"
                ip       = "10.10.10.202"
                login    = 
                password =
                label    =  Sisco App

                resource = "vHost" "claweb004"
                hostname = "claweb004.ctxfarm.com"
                ip       = "10.10.10.203-13"
                login    = 
                password =
                label    =  CLA dev

                resource = "vHost" "claweb005"
                hostname = "claweb005.ctxfarm.com"
                ip       = "10.10.10.204"
                login    = 
                password =
                label    =  CLA new-web
            }

            resource = "vApps" "@@DMZ_ctxweb" {

                resource = "vHost" "ctxweb001" 
                hostname = "ctxweb001.ctxfarm.com"
                ip       = "10.10.10.100"
                login    = 
                password =
                label    =  
                web_url  =  tms.hz-inova.com

                resource = "vHost" "ctxweb002" 
                hostname = "ctxweb002.ctxfarm.com"
                ip       = "10.10.10.101"
                login    = 
                password =
                label    =  
                web_url  =  app.ctxfarm.com

                resource = "vHost" "ctxweb003"
                hostname = "ctxweb003.ctxfarm.com"
                ip       = "10.10.10.102"
                login    = 
                password =
                label    =  
                web_url  =  teic.ctxfarm.com

                resource = "vHost" "ctxweb004"
                hostname = "ctxweb004.ctxfarm.com"
                ip       = "10.10.10.103"
                login    = 
                password =
                label    = 
                web_url  =  kt.ctxfarm.com

                resource = "vHost" "ctxweb005"
                hostname = "ctxweb005.ctxfarm.com"
                ip       = "10.10.10.104"
                login    = 
                password =
                label    =  
                web_url  =  app.websisco.com

                resource = "vHost" "ctxweb006"
                hostname = "ctxweb005.ctxfarm.com"
                ip       = "10.10.10.105"
                login    = 
                password =
                label    =  
                web_url  =  rapps.ctxfarm.com
            }

            resource = "vApps" "@@DMZ_sg" {

                resource = "vHost" "sg001"
                hostname = "sg001.ctxfarm.com"
                ip       = "10.10.10.20"
                login    = 
                password =
                label    = Citrix Secure Gateway
            }

            resource = "vApps" "@AD_license" {

                resource = "vHost" "cla6600" 
                hostname = "cla6600.ctxfarm.com"
                ip       = "192.168.48.10"
                login    =
                password =
                label    =  Citrix License Server
                os       = "Windows Server 2008"
                domain   = "ctxfarm.com"
                roles    = "File Services; Network Policy; Web Server IIS"
                features = "Group Policy Management"
                software = (

                    resource "sql" "MSQL2005" {

                    }

                    resource "ctx" "CitrixLicensing" {
                        
                    }
                        
                )

                resource = "vHost" "cladc20"
                hostname = "cladc20.ctxfarm.com"
                ip       = "192.168.56.191"
                login    = 
                password = 
                label    =  Domain Controller
                os       = "Windows Server 2008r2"
                domain   = "ctxfarm.com"
                roles    = "Domain Controller; AD Domain Services; DNS Server; RDP Services"
                features = "Group Policy Management"
                software =

                resource = "vHost" "cladc40"
                hostname = "cladc40.ctxfarm.com"
                ip       = "192.168.56.192"
                login    = 
                password = 
                label    =  Domain Controller 
                os       = "Windows Server 2012r2"
                domain   = "ctxfarm.com"
                roles    = "Domain Controller; AD Domain Services; DNS Server; File Storage Services; RDP Services"
                features = "Group Policy Management"
                software = (

                    resource "bck" "CommvaultProcesManager" {

                    }
                )
            }

            resource = "vApps" "@CTXapp6.x" {

                resource = "vHost" "ctxapp03"
                hostname = "ctxapp003.ctxfarm.com"
                ip       = "192.168.60.102"
                login    =
                password = 
                label    =

                resource = "vHost" "ctxapp05"
                hostname = "ctxapp005.ctxfarm.com"
                ip       = "192.168.60.104"
                login    = 
                password =
                label    =

                resource = "vHost" "ctxapp06"
                hostname = "ctxapp006.ctxfarm.com"
                ip       = "192.168.60.105"
                login    =
                password =
                label    =

                resource = "vHost" "ctxapp07"
                hostname = "ctxapp007.ctxfarm.com"
                ip       = "192.168.60.106"
                login    = 
                password =
                label    =

                resource = "vHost" "ctxapp08"
                hostname = "ctxapp008.ctxfarm.com"
                ip       = "192.168.60.107"
                login    =
                password = 
                label    =

                resource = "vHost" "ctxapp09"
                hostname = "ctxapp009.ctxfarm.com"
                ip       = "192.168.60.108"
                login    =
                password =
                label    =

                resource = "vHost" "ctxapp10"
                hostname = "ctxapp010.ctxfarm.com"
                ip       = "192.168.60.110"
                login    = 
                password = 
                label    =

                resource = "vHost" "ctxapp11"
                hostname = "ctxapp011.ctxfarm.com"
                ip       = "192.168.60.110"
                login    =
                password = 
                label    =
                os       = "Windows Server 2008r2"
                domain   = "ctxfarm.com"
                roles    = 
                features = 
                software = (

                    resource    "ctx" "CitrixXenApp" {
                        
                    }
            
                    resource    "ftp" "FilezillaFTPServer" {
                                ftpusr      "cla01"
                                pftpwd      "zA370Kc"
                    }
                    
                    resource    "mail" "hMailServer" {
                                login       "administrator"
                                password    "zA370Kc"    
                    }
               )

                resource = "vHost" "ctxapp12"
                hostname = "ctxapp012.ctxfarm.com"
                ip       = "192.168.60.111"
                login    = 
                password =
                label    =
                os       = "Windows Server 2008r2"
                domain   = "ctxfarm.com"
                roles    = 
                features = 
                software = (

                    resource    "ctx" "CitrixXenApp" {
                        
                    }
            }

            resource = "vApps" "@Eni" {

                resource = "vHost" "agpapp01"
                hostname = "agpapp01.ctxfarm.com"
                ip       = "192.168.40.210"
                login    = 
                password =
                label    =

                resource = "vHost" "farmagp02"
                hostname = "farmagp02.ctxfarm.com"
                ip       = "192.168.40.200"
                login    =
                password = 
                label    =
            }

            resource = "vApps" "@Maintenance" {

                resource = "vHost" "clamng002"
                hostname = "clamng002.ctxfarm.com"
                ip       = "192.168.60.11"
                login    = 
                password = 
                label    =
            }

            resource = "vApps" "@Services" {

                resource = "vHost" "clasrv001"
                hostname = "clasrv001.ctxfarm.com"
                ip       = "192.168.58.50"
                login    =
                password = 
                label    =
            }

            resource = "vApps" "@Sini" {
            }
        }

        resource = "resource-pool" "bv003957_work(bck60)" {

            resource = "vApps" "@CTXapp6" {

                resource = "vHost" "ctxapp001" 
                hostname = "ctxapp001.ctxfarm.com"
                ip       = "192.168.60.100"
                login    =
                password = 
                label    =
                os       =
                domain   =
                roles    =
                features =
                software = (

                )

                resource = "vHost" "ctxapp002"
                hostname = "ctxapp002.ctxfarm.com"
                ip       = "192.168.60.101"
                login    =
                password = 
                label    =

                resource = "vHost" "ctxapp004"
                hostname = "ctxapp004.ctxfarm.com"
                ip       = "192.168.60.103"
                login    =
                password =
                label    =
            }

            resource = "vApps" "@@@CADMatic_e-share" {

                resource = "vHost" "eshare"
                hostname = "eshare.cladom.com"
                ip       = "192.168.40.30"
                login    =
                password =
                label    = 
            }

            resource = "vApps" "@DB" {

                resource = "vHost" "cladb50"
                hostname = "cladb50.ctxfarm.com"
                ip       = "192.168.59.20"
                login    =
                password =
                label    =
                os       =
                domain   =
                roles    =
                features =
                software =

                resource = "vHost" "cladb60"
                hostname = "cladb60.ctxfarm.com"
                ip       = "192.168.59.25"
                login    = 
                password =
                label    =

                resource = "vHost" "cladb70"
                hostname = "cladb70.ctxfarm.com"
                ip       = "192.168.59.26"
                login    = 
                password = 
                label    = 
            }

            resource = "vApps" "@Elena" {

                resource = "vHost" "claeln001"
                hostname = "claeln001.ctxfarm.com"
                ip       = "192.168.58.100"
                login    =
                password =
                label    =
                os       =
                domain   =
                roles    =
                features =
                software = (
                )
            }

            resource = "vApps" "@STG-FTP" {

                resource = "vHost" "clastg001"
                hostname = "clastg001.ctxfarm.com"
                ip       = "192.168.59.30"
                login    = 
                password =
                label    =  Storage Server Windows
                os       = "Windows Server 2012r2"
                domain   = "ctxfarm.com"
                roles    = 
                features = 
                software = (

                    resource    "ftp" "cerberusFTP" {
                                login       "administrator"
                                password    "!FTPcerberusAdministrator!2017"
                                serverdb    "cladb50.ctxfarm.com"
                                database    "cerberusreporting"
                                usr_db      "usrcerberus"
                                pwd_db      "pwdcerberus"
                    }
                )
            }
                
        }
    }

    group = "resource-group" "ENI" {
      project  = "17000000-81"
      login    = "bv003964"
      password = "17*aMYLgirLIShhAsten"
      label    =  Accesso vCenter Supernap ENI

        resource = "resource-pool" "bv003964_develop(nobck)" {
        }

        resource = "resource-pool" "bv003964_work(bck30)" {
        }

        resource = "resource-pool" "bv003964_work(bck60)" {

            resource = "vHost" "bv396400"
            hostname = "bv396400.cladom.com"
            ip       = "192.168.40.20"
            login    = 
            password = 
            label    =

            resource = "vHost" "bv396402"
            hostname = "bv396402.cladom.com"
            ip       = "192.168.40.22"
            login    = 
            password = 
            label    =


            resource = "vHost" "bv3964dc"
            hostname = "bv3964dc.cladom.com"
            ip       = "192.168.40.10"
            login    = 
            password = 
            label    =
        }
    }
}


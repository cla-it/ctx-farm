#DOMAIN

	Customer ID: C00oisewt
	Primary Domain: cla-it.eu
	Primary Domain Verified: True
	Customer Creation Time: 2010-02-03T10:50:06.251Z
	Default Language: en-GB
	Address:
	 contactName: Bulla Federico
	 organizationName: Computer Line Associates s.r.l.
	 addressLine1: Strada Della Viggioletta n.8
	 locality: Piacenza
	 region: PC
	 postalCode: 29121
	 countryCode: IT
	Admin Secondary Email: federico.bulla@gmail.com
	User counts as of 2018-06-1:
	  G Suite Basic Licenses: 28
	  G Suite Basic Users: 28
	  Total Users: 28

#USERS

	adolfo.caccialanza@cla-it.eu
		First Name: Adolfo
		Last Name: Caccialanza
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 101514837003580980020
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2012-05-24T13:23:31.000Z
		Last login time: 2018-05-26T09:56:49.000Z
		Google Org Unit Path: /
	
		Photo URL: https://plus.google.com/_/focus/photos/private/AIbEiAIAAABDCLT2vpiph_KCFSILdmNhcmRfcGhvdG8qKGE1MjIxMGQ3YTBkNjNlZWQ0MGJjMDA0MzhmOTJlZGE5MDFlZjI0ZGMwAXldS5hS7SmTorhHLJh_dio74q0R
		
		Non-Editable Aliases:
		  adolfo.caccialanza@cla-it.com
		  adolfo.caccialanza@cla-it.it
		  adolfo.caccialanza@puma5.biz
		  adolfo.caccialanza@puma5.com
		  adolfo.caccialanza@puma5.eu
		  adolfo.caccialanza@puma5.info
		  adolfo.caccialanza@puma5.it
		  adolfo.caccialanza@cl2001.it
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	alessandro.garberi@cla-it.eu
		User: alessandro.garberi@cla-it.eu
		First Name: Alessandro
		Last Name: Garberi
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 113129015442739554306
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2013-07-15T09:27:15.000Z
		Last login time: 2018-05-31T08:09:11.000Z
		Google Org Unit Path: /
		
		Non-Editable Aliases:
		  alessandro.garberi@cla-it.com
		  alessandro.garberi@cla-it.it
		  alessandro.garberi@puma5.biz
		  alessandro.garberi@puma5.com
		  alessandro.garberi@puma5.eu
		  alessandro.garberi@puma5.info
		  alessandro.garberi@puma5.it
		  alessandro.garberi@cl2001.it
		Groups: (8)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   authentication@cla-it.eu / youtrack <authentication@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   jira-dev-team <jira-dev-team@cla-it.eu>
		   jobs@cla-it.eu <jobs@cla-it.eu>
		   software@cl2001.it <software@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	ana.dumitrescu@cla-it.eu
		First Name: Ana
		Last Name: Dumitrescu
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: True
		Suspension Reason: ADMIN
		Must Change Password: False
		Google Unique ID: 108341730359826042988
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2017-01-10T08:28:35.000Z
		Last login time: 2017-11-06T08:23:40.000Z
		Google Org Unit Path: /
		
		Non-Editable Aliases:
		  ana.dumitrescu@cl2001.it
		  ana.dumitrescu@cla-it.com
		  ana.dumitrescu@cla-it.it
		  ana.dumitrescu@puma5.biz
		  ana.dumitrescu@puma5.com
		  ana.dumitrescu@puma5.eu
		  ana.dumitrescu@puma5.info
		  ana.dumitrescu@puma5.it
		Groups: (1)
		   claro <claro@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	andrea.copelli@cla-it.eu
		First Name: Andrea
		Last Name: Copelli
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 113845778085895984696
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-10-22T14:16:15.000Z
		Last login time: 2018-05-31T15:53:03.000Z
		Google Org Unit Path: /
		
		Non-Editable Aliases:
		  andrea.copelli@cla-it.it
		  andrea.copelli@puma5.com
		  andrea.copelli@puma5.eu
		  andrea.copelli@puma5.info
		  andrea.copelli@puma5.biz
		  andrea.copelli@puma5.it
		  andrea.copelli@cla-it.com
		  andrea.copelli@cl2001.it
		
		Groups: (11)
		   allert@cla-it.eu / notification <allert@cla-it.eu>
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   efax@cla-it.eu <efax@cla-it.eu>
		   efaxcl2001@cl2001.it <efaxcl2001@cla-it.eu>
		   hsupport@cla-it.eu <hsupport@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   Info@cla-it.eu / cl2001.it <info@cla-it.eu>
		   lansupport@cla-it.eu / cl2001.it <lansupport@cla-it.eu>
		   providers@cl2001.it <providers@cla-it.eu>
		   software@cl2001.it <software@cla-it.eu>
		   wf@cla-it.eu <wf@cla-it.eu>
		Licenses:

		  Google-Apps-For-Business (G Suite Basic)
	andrea.repetti@cla-it.eu
		First Name: Andrea
		Last Name: Repetti
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 110772679293683513078
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2017-07-28T14:35:32.000Z
		Last login time: 2018-05-18T13:48:00.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/private/AIbEiAIAAABECPbt2Jnd14_AlQEiC3ZjYXJkX3Bob3RvKihkMmMyMDA0NGVmMmJhZDc3NDVkZGY2OWY2MzBjNDllY2M3ZDA1MGFlMAHYTWn9cAVqP8ZPYq0DyEV834CW4g
		
		Non-Editable Aliases:
		  andrea.repetti@cl2001.it
		  andrea.repetti@cla-it.com
		  andrea.repetti@cla-it.it
		  andrea.repetti@puma5.biz
		  andrea.repetti@puma5.com
		  andrea.repetti@puma5.eu
		  andrea.repetti@puma5.info
		  andrea.repetti@puma5.it
		Groups: (7)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   confluence-team <confluence-team@cla-it.eu>
		   hsupport@cla-it.eu <hsupport@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   lansupport@cla-it.eu / cl2001.it <lansupport@cla-it.eu>
		   software@cl2001.it <software@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	beatrice.milanesi@cla-it.eu
		First Name: Beatrice
		Last Name: Milanesi
		Is a Super Admin: True
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 103108278014828471062
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2011-02-24T09:17:48.000Z
		Last login time: 2018-06-01T07:01:45.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/public/AIbEiAIAAABDCJbGh8itzLSRKyILdmNhcmRfcGhvdG8qKDM5MDZiZjUwZjU5ZGQyYzVjYzk2ZTJkNDc2YjIzYWM5ZjI0OWM1MmMwAUIhAuNTAon8lNjqNHqpDcU1YLg1
		
		Non-Editable Aliases:
		  beatrice.milanesi@cla-it.it
		  beatrice.milanesi@puma5.com
		  beatrice.milanesi@puma5.eu
		  beatrice.milanesi@puma5.info
		  beatrice.milanesi@puma5.biz
		  beatrice.milanesi@puma5.it
		  beatrice.milanesi@cla-it.com
		  beatrice.milanesi@cl2001.it
		Groups: (17)
		   amministrazione01@cl2001.it <amministrazione01@cla-it.eu>
		   amministrazione@cla-it.eu <amministrazione@cla-it.eu>
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   australia@cla-it.eu <australia@cla-it.eu>
		   convention@cla-it.eu <convention@cla-it.eu>
		   efax@cla-it.eu <efax@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   india@cla-it.eu <india@cla-it.eu>
		   Info@cla-it.eu / cl2001.it <info@cla-it.eu>
		   iran@cla-it.eu <iran@cla-it.eu>
		   legal@cla-it.eu <legal@cla-it.eu>
		   marketing@cla-it.eu <marketing@cla-it.eu>
		   netherlands@cla-it.eu <netherlands@cla-it.eu>
		   sales@cla-it.eu <sales@cla-it.eu>
		   singapore@cla-it.eu <singapore@cla-it.eu>
		   southafrica@cla-it.eu <southafrica@cla-it.eu>
		   southkorea@cla-it.eu <southkorea@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	daniela.nitu@cla-it.eu
		First Name: Daniela
		Last Name: Nitu
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 114265789371772649366
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2015-02-11T10:12:13.000Z
		Last login time: 2018-05-31T12:08:51.000Z
		Google Org Unit Path: /
		
		Email Aliases:
		  gabriella@cla-it.eu
		Non-Editable Aliases:
		  daniela.nitu@cla-it.com
		  daniela.nitu@cla-it.it
		  daniela.nitu@puma5.biz
		  daniela.nitu@puma5.com
		  daniela.nitu@puma5.eu
		  daniela.nitu@puma5.info
		  daniela.nitu@puma5.it
		  gabriella@cla-it.com
		  gabriella@cla-it.it
		  gabriella@puma5.biz
		  gabriella@puma5.com
		  gabriella@puma5.eu
		  gabriella@puma5.info
		  gabriella@puma5.it
		  daniela.nitu@cl2001.it
		  gabriella@cl2001.it
		Groups: (3)
		   cla4eni@cla-it.eu <cla4eni@cla-it.eu>
		   claro <claro@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	davide.delfanti@cla-it.eu
		User: davide.delfanti@cla-it.eu
		First Name: Davide
		Last Name: Delfanti
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 100991262617913845464
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2017-12-04T08:08:00.000Z
		Last login time: 2018-05-23T11:51:43.000Z
		Google Org Unit Path: /
		
		Organizations:
		 description: 
		 title: 
		 primary: True
		 costCenter: 
		 department: 
		
		External IDs:
		 type: organization
		 value: 
		
		Non-Editable Aliases:
		  davide.delfanti@cl2001.it
		  davide.delfanti@cla-it.com
		  davide.delfanti@cla-it.it
		  davide.delfanti@puma5.biz
		  davide.delfanti@puma5.com
		  davide.delfanti@puma5.eu
		  davide.delfanti@puma5.info
		  davide.delfanti@puma5.it
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	eliana.franchi@cla-it.eu
		First Name: Eliana
		Last Name: Franchi
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 108130907263273459683
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2016-08-29T10:57:58.000Z
		Last login time: 2018-06-01T12:03:05.000Z
		Google Org Unit Path: /
		
		Email Aliases:
		  franchi.eliana@cla-it.eu
		  eliana@cla-it.eu
		Non-Editable Aliases:
		  franchi.eliana@cla-it.com
		  franchi.eliana@cla-it.it
		  franchi.eliana@puma5.biz
		  franchi.eliana@puma5.com
		  franchi.eliana@puma5.eu
		  franchi.eliana@puma5.info
		  franchi.eliana@puma5.it
		  eliana.franchi@cla-it.com
		  eliana.franchi@cla-it.it
		  eliana.franchi@puma5.biz
		  eliana.franchi@puma5.com
		  eliana.franchi@puma5.eu
		  eliana.franchi@puma5.info
		  eliana.franchi@puma5.it
		  eliana.franchi@cl2001.it
		  eliana@cl2001.it
		  eliana@cla-it.com
		  eliana@cla-it.it
		  eliana@puma5.biz
		  eliana@puma5.com
		  eliana@puma5.eu
		  eliana@puma5.info
		  eliana@puma5.it
		  franchi.eliana@cl2001.it
		Groups: (9)
		   amministrazione01@cl2001.it <amministrazione01@cla-it.eu>
		   amministrazione@cla-it.eu <amministrazione@cla-it.eu>
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   efaxcl2001@cl2001.it <efaxcl2001@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   Info@cla-it.eu / cl2001.it <info@cla-it.eu>
		   infoprivacy <infoprivacy@cla-it.eu>
		   software@cl2001.it <software@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	federico.bulla@cla-it.eu
		First Name: Federico
		Last Name: Bulla
		Is a Super Admin: True
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 102133002911741367748
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: False
		Creation Time: 2010-02-03T10:53:18.000Z
		Last login time: 2018-06-01T06:38:32.000Z
		Google Org Unit Path: /
		
		Notes:
		 contentType: text_html
		 value:
		
		Email Aliases:
		  federico@cla-it.eu
		  fedfant@cla-it.eu
		  webmaster@cla-it.eu
		  hostmaster@cla-it.eu
		  admin@cla-it.eu
		Non-Editable Aliases:
		  federico@cla-it.it
		  federico.bulla@cla-it.it
		  fedfant@cla-it.it
		  federico@puma5.com
		  federico.bulla@puma5.com
		  fedfant@puma5.com
		  federico@puma5.eu
		  federico.bulla@puma5.eu
		  fedfant@puma5.eu
		  federico@puma5.info
		  federico.bulla@puma5.info
		  fedfant@puma5.info
		  federico@puma5.biz
		  federico.bulla@puma5.biz
		  fedfant@puma5.biz
		  federico@puma5.it
		  federico.bulla@puma5.it
		  fedfant@puma5.it
		  federico@cla-it.com
		  federico.bulla@cla-it.com
		  fedfant@cla-it.com
		  webmaster@cla-it.com
		  webmaster@cla-it.it
		  webmaster@puma5.biz
		  webmaster@puma5.com
		  webmaster@puma5.eu
		  webmaster@puma5.info
		  webmaster@puma5.it
		  hostmaster@cla-it.com
		  hostmaster@cla-it.it
		  hostmaster@puma5.biz
		  hostmaster@puma5.com
		  hostmaster@puma5.eu
		  hostmaster@puma5.info
		  hostmaster@puma5.it
		  admin@cla-it.com
		  admin@cla-it.it
		  admin@puma5.biz
		  admin@puma5.com
		  admin@puma5.eu
		  admin@puma5.info
		  admin@puma5.it
		  federico.bulla@cl2001.it
		  admin@cl2001.it
		  federico@cl2001.it
		  fedfant@cl2001.it
		  hostmaster@cl2001.it
		  webmaster@cl2001.it
		Groups: (27)
		   allert@cla-it.eu / notification <allert@cla-it.eu>
		   amministrazione01@cl2001.it <amministrazione01@cla-it.eu>
		   amministrazione@cla-it.eu <amministrazione@cla-it.eu>
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   authentication@cla-it.eu / youtrack <authentication@cla-it.eu>
		   bank.beep@cla-it.eu <bank.beep@cla-it.eu>
		   confluence-team <confluence-team@cla-it.eu>
		   efax@cla-it.eu <efax@cla-it.eu>
		   efaxcl2001@cl2001.it <efaxcl2001@cla-it.eu>
		   farmsupport@cla-it.eu <farmsupport@cla-it.eu>
		   hsupport@cla-it.eu <hsupport@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   Info@cla-it.eu / cl2001.it <info@cla-it.eu>
		   infoprivacy <infoprivacy@cla-it.eu>
		   jobs@cla-it.eu <jobs@cla-it.eu>
		   lansupport@cla-it.eu / cl2001.it <lansupport@cla-it.eu>
		   presenze@cla-it.eu <presenze@cla-it.eu>
		   providers@cl2001.it <providers@cla-it.eu>
		   sales@cla-it.eu <sales@cla-it.eu>
		   software@cl2001.it <software@cla-it.eu>
		   Support Cloud <support.cloud@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		   temil@cla-it.eu <temil@cla-it.eu>
		   wf@cla-it.eu <wf@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	gabriele.repetti@cla-it.eu
		First Name: Gabriele
		Last Name: Repetti
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 107436406829018137911
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-10-22T14:25:39.000Z
		Last login time: 2018-05-28T16:05:52.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/public/AIbEiAIAAABDCLeKqcfHk9uZZyILdmNhcmRfcGhvdG8qKDc3NzkzNzllZDkwZjA2NDZmMWZmYjg5NDA3NzlkOWEwMDExNDRlZjMwAUoNsGN8iufTLMxcYXxyskE1_At0
		
		Non-Editable Aliases:
		  gabriele.repetti@cla-it.it
		  gabriele.repetti@puma5.com
		  gabriele.repetti@puma5.eu
		  gabriele.repetti@puma5.info
		  gabriele.repetti@puma5.biz
		  gabriele.repetti@puma5.it
		  gabriele.repetti@cla-it.com
		  gabriele.repetti@cl2001.it
		Groups: (8)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   cosmo.development@cla-it.eu <cosmo.development@cla-it.eu>
		   customer.service@cla-it.eu <customer.service@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   jira-dev-team <jira-dev-team@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	giacomo.barbieri@cla-it.eu
		First Name: Giacomo
		Last Name: Barbieri
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 112697181202195642413
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2011-12-16T16:51:24.000Z
		Last login time: 2018-05-30T08:14:13.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/public/AIbEiAIAAABECK3wja3-4NyasAEiC3ZjYXJkX3Bob3RvKigwZGJmOGFiZmQ4OWMyMGJkODkzYzkzYzA2NDI2ZDYzN2U4YzljMWFiMAEY5eZWlBAbRIgF5gLSrhu-KgWZ2w
		
		Notes:
		 contentType: text_html
		 value:
		
		Non-Editable Aliases:
		  giacomo.barbieri@cla-it.com
		  giacomo.barbieri@cla-it.it
		  giacomo.barbieri@puma5.biz
		  giacomo.barbieri@puma5.com
		  giacomo.barbieri@puma5.eu
		  giacomo.barbieri@puma5.info
		  giacomo.barbieri@puma5.it
		  giacomo.barbieri@cl2001.it
		Groups: (6)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   cosmo.development@cla-it.eu <cosmo.development@cla-it.eu>
		   customer.service@cla-it.eu <customer.service@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	gianclaudio.lertora@cla-it.eu
		First Name: Gianclaudio
		Last Name: Lertora
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: True
		Suspension Reason: ADMIN
		Must Change Password: False
		Google Unique ID: 118060361787421739568
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-10-22T15:42:21.000Z
		Last login time: 2017-07-07T14:25:20.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/public/AIbEiAIAAABECLCUv7K569LR-gEiC3ZjYXJkX3Bob3RvKihhNDU3MzY4ZjMxMWRiNDkyNWRlMzQ5NThjNTNiM2JkMGM5ZjU2ZDllMAHv4PIKXxw7zyAMekuBOBYjfax_Qg
		
		Non-Editable Aliases:
		  gianclaudio.lertora@cla-it.it
		  gianclaudio.lertora@puma5.com
		  gianclaudio.lertora@puma5.eu
		  gianclaudio.lertora@puma5.info
		  gianclaudio.lertora@puma5.biz
		  gianclaudio.lertora@puma5.it
		  gianclaudio.lertora@cla-it.com
		  gianclaudio.lertora@cl2001.it
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	gianmario.tagliaretti@cla-it.eu
		First Name: Gian Mario
		Last Name: Tagliaretti
		Is a Super Admin: True
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 101624488079146448225
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-02-03T11:39:16.000Z
		Last login time: 2018-06-01T08:16:20.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/public/AIbEiAIAAABDCOHqhZvR6dXFFiILdmNhcmRfcGhvdG8qKDc2Y2QxMzdhNWE0NDFmZWU3ZDJmMDMxMmY2M2IzMTgyOTBmZDdkNTQwAaDje3wMUEbpq6u6g0OsSY-QPiS9
		
		Notes:
		 contentType: text_html
		 value:
		
		Email Aliases:
		  mario@cla-it.eu
		Non-Editable Aliases:
		  gianmario.tagliaretti@cla-it.it
		  gianmario.tagliaretti@puma5.com
		  gianmario.tagliaretti@puma5.eu
		  gianmario.tagliaretti@puma5.info
		  gianmario.tagliaretti@puma5.biz
		  gianmario.tagliaretti@puma5.it
		  gianmario.tagliaretti@cla-it.com
		  mario@cla-it.com
		  mario@cla-it.it
		  mario@puma5.biz
		  mario@puma5.com
		  mario@puma5.eu
		  mario@puma5.info
		  mario@puma5.it
		  gianmario.tagliaretti@cl2001.it
		  mario@cl2001.it
		Groups: (32)
		   amministrazione@cla-it.eu <amministrazione@cla-it.eu>
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   australia@cla-it.eu <australia@cla-it.eu>
		   authentication@cla-it.eu / youtrack <authentication@cla-it.eu>
		   cla4eni@cla-it.eu <cla4eni@cla-it.eu>
		   convention@cla-it.eu <convention@cla-it.eu>
		   cosmo.development@cla-it.eu <cosmo.development@cla-it.eu>
		   customer.service@cla-it.eu <customer.service@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   india@cla-it.eu <india@cla-it.eu>
		   Info@cla-it.eu / cl2001.it <info@cla-it.eu>
		   iran@cla-it.eu <iran@cla-it.eu>
		   jobs@cla-it.eu <jobs@cla-it.eu>
		   legal@cla-it.eu <legal@cla-it.eu>
		   marketing@cla-it.eu <marketing@cla-it.eu>
		   netherlands@cla-it.eu <netherlands@cla-it.eu>
		   no-reply@cla-it.eu <no-reply@cla-it.eu>
		   presenze@cla-it.eu <presenze@cla-it.eu>
		   sales@cla-it.eu <sales@cla-it.eu>
		   services@cla-it.eu <services@cla-it.eu>
		   singapore@cla-it.eu <singapore@cla-it.eu>
		   software@cl2001.it <software@cla-it.eu>
		   southafrica@cla-it.eu <southafrica@cla-it.eu>
		   southkorea@cla-it.eu <southkorea@cla-it.eu>
		   Support Bidman <support.bidman@cla-it.eu>
		   Support Cosmo <support.cosmo@cla-it.eu>
		   Support TMS <support.tms@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		   temp@cla-it.eu <temp@cla-it.eu>
		   wf@cla-it.eu <wf@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
		  Google-Drive-storage-20GB (Google Drive Storage 20GB)
	giuseppe.schifitto@cla-it.eu
		First Name: Giuseppe
		Last Name: Schifitto
		Is a Super Admin: True
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 112699887948612034722
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2018-03-12T07:59:03.000Z
		Last login time: 2018-06-01T08:17:20.000Z
		Google Org Unit Path: /CL2001
		
		Photo URL: https://plus.google.com/_/focus/photos/private/AIbEiAIAAABECKLBo8LWmcSfsAEiC3ZjYXJkX3Bob3RvKig3NWU3NDczYWFkZDc5NmY1MDRhY2I0YWVhZjRhMWZkN2Y2ODU3ODA4MAHkIDCCbepMIsQMf6Em51Pc0O19VA
		
		Organizations:
		 description: 
		 title: 
		 primary: True
		 costCenter: 
		 department: 
		
		External IDs:
		 type: organization
		 value: 
		
		Non-Editable Aliases:
		  giuseppe.schifitto@cl2001.it
		  giuseppe.schifitto@cla-it.com
		  giuseppe.schifitto@cla-it.it
		  giuseppe.schifitto@puma5.biz
		  giuseppe.schifitto@puma5.com
		  giuseppe.schifitto@puma5.eu
		  giuseppe.schifitto@puma5.info
		  giuseppe.schifitto@puma5.it
		Groups: (16)
		   allert@cla-it.eu / notification <allert@cla-it.eu>
		   authentication@cla-it.eu / youtrack <authentication@cla-it.eu>
		   bank.beep@cla-it.eu <bank.beep@cla-it.eu>
		   confluence-team <confluence-team@cla-it.eu>
		   efax@cla-it.eu <efax@cla-it.eu>
		   farmsupport@cla-it.eu <farmsupport@cla-it.eu>
		   hsupport@cla-it.eu <hsupport@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   lansupport@cla-it.eu / cl2001.it <lansupport@cla-it.eu>
		   Support Cloud <support.cloud@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		   temil@cla-it.eu <temil@cla-it.eu>
		   wf@cla-it.eu <wf@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	giuseppe.siboni@cla-it.eu
		First Name: Giuseppe
		Last Name: Siboni
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 104531050002155595012
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-10-22T15:42:49.000Z
		Last login time: 2018-05-30T07:26:01.000Z
		Google Org Unit Path: /
	
		Photo URL: https://plus.google.com/_/focus/photos/private/AIbEiAIAAABDCITKkfCGv-HwPiILdmNhcmRfcGhvdG8qKDI3ZDc2YzhmNDA3OTVmNGY1Y2IzMzY1MTM5ZWVlYTg1MWZlZGZmM2IwATBCiTqgQ4jEVMgYO-Yt9kXtRsuu
	
		Non-Editable Aliases:
		  giuseppe.siboni@cla-it.it
		  giuseppe.siboni@puma5.com
		  giuseppe.siboni@puma5.eu
		  giuseppe.siboni@puma5.info
		  giuseppe.siboni@puma5.biz
		  giuseppe.siboni@puma5.it
		  giuseppe.siboni@cla-it.com
		  giuseppe.siboni@cl2001.it
		Groups: (6)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   jira-dev-team <jira-dev-team@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	jonathan.amodeo@cla-it.eu
		First Name: Jonathan
		Last Name: Amodeo
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 103489258623596882203
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2016-08-29T11:08:13.000Z
		Last login time: 2018-06-01T11:15:15.000Z
		Google Org Unit Path: /
		
		Email Aliases:
		  jonathan@cla-it.eu
		Non-Editable Aliases:
		  jonathan.amodeo@cla-it.com
		  jonathan.amodeo@cla-it.it
		  jonathan.amodeo@puma5.biz
		  jonathan.amodeo@puma5.com
		  jonathan.amodeo@puma5.eu
		  jonathan.amodeo@puma5.info
		  jonathan.amodeo@puma5.it
		  jonathan.amodeo@cl2001.it
		  jonathan@cl2001.it
		  jonathan@cla-it.com
		  jonathan@cla-it.it
		  jonathan@puma5.biz
		  jonathan@puma5.com
		  jonathan@puma5.eu
		  jonathan@puma5.info
		  jonathan@puma5.it
		Groups: (5)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   efaxcl2001@cl2001.it <efaxcl2001@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   software@cl2001.it <software@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
		laura.figliossi@cla-it.eu
		First Name: Laura
		Last Name: Figliossi
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 107770864404972561383
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2016-08-29T09:44:17.000Z
		Last login time: 2018-06-01T12:18:09.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/private/AIbEiAIAAABDCOfH98CSgOrrayILdmNhcmRfcGhvdG8qKGZiNTRhNGEyOWFkZGQwMjc4ZWMyOGVhM2YwZDYwMGFiOTFjMmQ5YTUwAX63Ok8CAwonrc2zVhJ9pJ-qHxM7
		
		Email Aliases:
		  supporto@cla-it.eu
		  supportoclienti@cla-it.eu
		Non-Editable Aliases:
		  laura.figliossi@cla-it.com
		  laura.figliossi@cla-it.it
		  laura.figliossi@puma5.biz
		  laura.figliossi@puma5.com
		  laura.figliossi@puma5.eu
		  laura.figliossi@puma5.info
		  laura.figliossi@puma5.it
		  laura.figliossi@cl2001.it
		  supporto@cl2001.it
		  supporto@cla-it.com
		  supporto@cla-it.it
		  supporto@puma5.biz
		  supporto@puma5.com
		  supporto@puma5.eu
		  supporto@puma5.info
		  supporto@puma5.it
		  supportoclienti@cl2001.it
		  supportoclienti@cla-it.com
		  supportoclienti@cla-it.it
		  supportoclienti@puma5.biz
		  supportoclienti@puma5.com
		  supportoclienti@puma5.eu
		  supportoclienti@puma5.info
		  supportoclienti@puma5.it
		Groups: (6)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   efaxcl2001@cl2001.it <efaxcl2001@cla-it.eu>
		   hsupport@cla-it.eu <hsupport@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   Info@cla-it.eu / cl2001.it <info@cla-it.eu>
		   providers@cl2001.it <providers@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	maddalena.losi@cla-it.eu
		First Name: Maddalena
		Last Name: Losi
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 111536015496525537672
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2011-08-24T12:49:55.000Z
		Last login time: 2018-05-17T17:32:40.000Z
		Google Org Unit Path: /
	
		Photo URL: https://plus.google.com/_/focus/photos/private/AIbEiAIAAABECIjDuY6Un4qMoAEiC3ZjYXJkX3Bob3RvKihkNDE4MGM5NzE0NTAyMGQzMmI2ZDVhOWM5OTY2N2IzYWNkNGM0NjUwMAGq764wrSTrhq30WE6xkMNcRjarXw
	
		Non-Editable Aliases:
		  maddalena.losi@cla-it.com
		  maddalena.losi@cla-it.it
		  maddalena.losi@puma5.biz
		  maddalena.losi@puma5.com
		  maddalena.losi@puma5.eu
		  maddalena.losi@puma5.info
		  maddalena.losi@puma5.it
		  maddalena.losi@cl2001.it
		Groups: (7)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   customer.service@cla-it.eu <customer.service@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   services@cla-it.eu <services@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	nicoletta.poggi@cla-it.eu
		First Name: Nicoletta
		Last Name: Poggi
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 111918955596179693407
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-10-22T15:43:44.000Z
		Last login time: 2018-05-21T07:12:29.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/public/AIbEiAIAAABECN--x5_y3qi0pQEiC3ZjYXJkX3Bob3RvKig5N2M4ZjIyOGRiZDg4ODNjODRhM2FmN2E1YTA5NDYyMTZkMDczZjZkMAEkqvB-qoC-smEoqRJ_-dBG_vjgeA
		
		Non-Editable Aliases:
		  nicoletta.poggi@cla-it.it
		  nicoletta.poggi@puma5.com
		  nicoletta.poggi@puma5.eu
		  nicoletta.poggi@puma5.info
		  nicoletta.poggi@puma5.biz
		  nicoletta.poggi@puma5.it
		  nicoletta.poggi@cla-it.com
		  nicoletta.poggi@cl2001.it
		Groups: (12)
		   amministrazione01@cl2001.it <amministrazione01@cla-it.eu>
		   amministrazione@cla-it.eu <amministrazione@cla-it.eu>
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   customer.service@cla-it.eu <customer.service@cla-it.eu>
		   efax@cla-it.eu <efax@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   Info@cla-it.eu / cl2001.it <info@cla-it.eu>
		   marketing@cla-it.eu <marketing@cla-it.eu>
		   sales@cla-it.eu <sales@cla-it.eu>
		   services@cla-it.eu <services@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	oscar.bergonzi@cla-it.eu
		First Name: Oscar
		Last Name: Bergonzi
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 100469308937140849720
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-10-22T15:44:36.000Z
		Last login time: 2018-05-24T04:56:14.000Z
		Google Org Unit Path: /
		
		Email Aliases:
		  oscar@cla-it.eu
		  CLA-no-reply@cla-it.eu
		  support-no-reply@cla-it.eu
		Non-Editable Aliases:
		  oscar.bergonzi@cla-it.it
		  oscar.bergonzi@puma5.com
		  oscar.bergonzi@puma5.eu
		  oscar.bergonzi@puma5.info
		  oscar.bergonzi@puma5.biz
		  oscar.bergonzi@puma5.it
		  oscar.bergonzi@cla-it.com
		  oscar@cla-it.com
		  oscar@cla-it.it
		  oscar@puma5.biz
		  oscar@puma5.com
		  oscar@puma5.eu
		  oscar@puma5.info
		  oscar@puma5.it
		  CLA-no-reply@cla-it.com
		  CLA-no-reply@cla-it.it
		  CLA-no-reply@puma5.biz
		  CLA-no-reply@puma5.com
		  CLA-no-reply@puma5.eu
		  CLA-no-reply@puma5.info
		  CLA-no-reply@puma5.it
		  support-no-reply@cla-it.com
		  support-no-reply@cla-it.it
		  support-no-reply@puma5.biz
		  support-no-reply@puma5.com
		  support-no-reply@puma5.eu
		  support-no-reply@puma5.info
		  support-no-reply@puma5.it
		  oscar.bergonzi@cl2001.it
		  CLA-no-reply@cl2001.it
		  oscar@cl2001.it
		  support-no-reply@cl2001.it
		Groups: (6)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   cla4eni@cla-it.eu <cla4eni@cla-it.eu>
		   cosmo.development@cla-it.eu <cosmo.development@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   services@cla-it.eu <services@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		Licenses:
	  	Google-Apps-For-Business (G Suite Basic)
	paolo.porcari@cla-it.eu
		First Name: Paolo
		Last Name: Porcari
		Is a Super Admin: True
		Is Delegated Admin: False
		2-step enrolled: True
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 107321770681241352048
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2017-09-04T06:39:56.000Z
		Last login time: 2018-05-31T15:07:39.000Z
		Google Org Unit Path: /CL2001
		
		Photo URL: https://plus.google.com/_/focus/photos/public/AIbEiAIAAABDCPDuirLG9InOZSILdmNhcmRfcGhvdG8qKDMyMDA4NjZkNmE3N2Y4NGIzYzEzN2UwYzAyNWJjZjZkOTRiNmZkMjYwAUW-4PM4mN3Ae_uFYkfR9RCUQ7uB
		
		Non-Editable Aliases:
		  paolo.porcari@cl2001.it
		  paolo.porcari@cla-it.com
		  paolo.porcari@cla-it.it
		  paolo.porcari@puma5.biz
		  paolo.porcari@puma5.com
		  paolo.porcari@puma5.eu
		  paolo.porcari@puma5.info
		  paolo.porcari@puma5.it
		Groups: (8)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   confluence-team <confluence-team@cla-it.eu>
		   efaxcl2001@cl2001.it <efaxcl2001@cla-it.eu>
		   hsupport@cla-it.eu <hsupport@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   lansupport@cla-it.eu / cl2001.it <lansupport@cla-it.eu>
		   software@cl2001.it <software@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	patrick.nani@cla-it.eu
		First Name: Patrick
		Last Name: Nani
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 102138807297801538792
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-10-22T15:45:13.000Z
		Last login time: 2018-06-01T10:50:08.000Z
		Google Org Unit Path: /
	
		Non-Editable Aliases:
		  patrick.nani@cla-it.it
		  patrick.nani@puma5.com
		  patrick.nani@puma5.eu
		  patrick.nani@puma5.info
		  patrick.nani@puma5.biz
		  patrick.nani@puma5.it
		  patrick.nani@cla-it.com
		  patrick.nani@cl2001.it
		Groups: (6)
		   cosmo.development@cla-it.eu <cosmo.development@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   services@cla-it.eu <services@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		   temil@cla-it.eu <temil@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	riccardo.maggiore@cla-it.eu
		First Name: Riccardo
		Last Name: Maggiore
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 108826458789556802220
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2016-04-14T13:52:58.000Z
		Last login time: 2018-06-01T13:18:45.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/private/AIbEiAIAAABDCKylocGSsPi-eiILdmNhcmRfcGhvdG8qKGJlMTgwOGU5M2Q4NjY5ZjYyMDMzMzQwNDliOTMyNDVhZGEzNjUxYmMwAU4NT5fxBdizs96irJg8xT05Nftu
		
		Non-Editable Aliases:
		  riccardo.maggiore@cla-it.com
		  riccardo.maggiore@cla-it.it
		  riccardo.maggiore@puma5.biz
		  riccardo.maggiore@puma5.com
		  riccardo.maggiore@puma5.eu
		  riccardo.maggiore@puma5.info
		  riccardo.maggiore@puma5.it
		  riccardo.maggiore@cl2001.it
		Groups: (4)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   cla4eni@cla-it.eu <cla4eni@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   services@cla-it.eu <services@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	stefano.cavanna@cla-it.eu
		First Name: Stefano
		Last Name: Cavanna
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 112241082665717942174
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2016-10-28T12:55:34.000Z
		Last login time: 2018-05-25T14:20:18.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/public/AIbEiAIAAABECJ6PvaLB-sPwqQEiC3ZjYXJkX3Bob3RvKig0YjRkZDkwMGZmM2NhZDU0NzJkMDExMzUzMDUyMzYwZDBhNThjYjJhMAEtj_5M9Xrpl6LlQ8gq0BhyhbWm1Q
		
		Email Aliases:
		  stefano@cla-it.eu
		Non-Editable Aliases:
		  stefano.cavanna@cl2001.it
		  stefano.cavanna@cla-it.com
		  stefano.cavanna@cla-it.it
		  stefano.cavanna@puma5.biz
		  stefano.cavanna@puma5.com
		  stefano.cavanna@puma5.eu
		  stefano.cavanna@puma5.info
		  stefano.cavanna@puma5.it
		  stefano@cl2001.it
		  stefano@cla-it.com
		  stefano@cla-it.it
		  stefano@puma5.biz
		  stefano@puma5.com
		  stefano@puma5.eu
		  stefano@puma5.info
		  stefano@puma5.it
		Groups: (4)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   hsupport@cla-it.eu <hsupport@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   lansupport@cla-it.eu / cl2001.it <lansupport@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	tina.germino@cla-it.eu
		First Name: Tina
		Last Name: Germino
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 101334783530842417275
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-10-22T15:46:17.000Z
		Last login time: 2018-05-28T11:54:05.000Z
		Google Org Unit Path: /
		
		Non-Editable Aliases:
		  tina.germino@cla-it.it
		  tina.germino@puma5.com
		  tina.germino@puma5.eu
		  tina.germino@puma5.info
		  tina.germino@puma5.biz
		  tina.germino@puma5.it
		  tina.germino@cla-it.com
		  tina.germino@cl2001.it
		Groups: (10)
		   amministrazione01@cl2001.it <amministrazione01@cla-it.eu>
		   amministrazione@cla-it.eu <amministrazione@cla-it.eu>
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   bank.beep@cla-it.eu <bank.beep@cla-it.eu>
		   efax@cla-it.eu <efax@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   Info@cla-it.eu / cl2001.it <info@cla-it.eu>
		   presenze@cla-it.eu <presenze@cla-it.eu>
		   wf@cla-it.eu <wf@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	tommaso.sala@cla-it.eu
		First Name: Tommaso
		Last Name: Sala
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 108110035873337364045
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2012-01-16T16:35:42.000Z
		Last login time: 2018-06-01T14:07:56.000Z
		Google Org Unit Path: /
		
		Photo URL: https://plus.google.com/_/focus/photos/public/AIbEiAIAAABDCM2cy4-H1ajGcCILdmNhcmRfcGhvdG8qKGYzYWViMmNiZDIwMWIwOTBlMjIwM2E2NWE0MGY2NGE2ZWFjMWE3ZmQwAfpRrhYxHctwnCr_dI-ymBeS1_7r
		
		Non-Editable Aliases:
		  tommaso.sala@cla-it.com
		  tommaso.sala@cla-it.it
		  tommaso.sala@puma5.biz
		  tommaso.sala@puma5.com
		  tommaso.sala@puma5.eu
		  tommaso.sala@puma5.info
		  tommaso.sala@puma5.it
		  tommaso.sala@cl2001.it
		Groups: (6)
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   customer.service@cla-it.eu <customer.service@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)
	ubaldo.bulla@cla-it.eu
		First Name: Ubaldo
		Last Name: Bulla
		Is a Super Admin: False
		Is Delegated Admin: False
		2-step enrolled: False
		2-step enforced: False
		Has Agreed to Terms: True
		IP Whitelisted: False
		Account Suspended: False
		Must Change Password: False
		Google Unique ID: 107307147917991788182
		Customer ID: C00oisewt
		Mailbox is setup: True
		Included in GAL: True
		Creation Time: 2010-02-03T11:21:39.000Z
		Last login time: 2018-05-28T05:48:54.000Z
		Google Org Unit Path: /
		
		Non-Editable Aliases:
		  ubaldo.bulla@cla-it.it
		  ubaldo.bulla@puma5.com
		  ubaldo.bulla@puma5.eu
		  ubaldo.bulla@puma5.info
		  ubaldo.bulla@puma5.biz
		  ubaldo.bulla@puma5.it
		  ubaldo.bulla@cla-it.com
		  ubaldo.bulla@cl2001.it
		Groups: (18)
		   amministrazione01@cl2001.it <amministrazione01@cla-it.eu>
		   amministrazione@cla-it.eu <amministrazione@cla-it.eu>
		   assenze@cla-it.eu <assenze@cla-it.eu>
		   cla4eni@cla-it.eu <cla4eni@cla-it.eu>
		   confluence-team <confluence-team@cla-it.eu>
		   development@cla-it.eu <development@cla-it.eu>
		   incl2001@cl2001.it <incl2001@cla-it.eu>
		   incla@cla-it.eu <incla@cla-it.eu>
		   india@cla-it.eu <india@cla-it.eu>
		   Info@cla-it.eu / cl2001.it <info@cla-it.eu>
		   jobs@cla-it.eu <jobs@cla-it.eu>
		   presenze@cla-it.eu <presenze@cla-it.eu>
		   sales@cla-it.eu <sales@cla-it.eu>
		   services@cla-it.eu <services@cla-it.eu>
		   software@cl2001.it <software@cla-it.eu>
		   Support SPC Puma <spcpuma@cla-it.eu>
		   support@cla-it.eu <support@cla-it.eu>
		   sw@cla-it.eu <sw@cla-it.eu>
		Licenses:
		  Google-Apps-For-Business (G Suite Basic)

#GROUPS

	allert@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  alert@cla-it.com
		  alert@cla-it.it
		  alert@puma5.biz
		  alert@puma5.com
		  alert@puma5.eu
		  alert@puma5.info
		  alert@puma5.it
		  allert@cla-it.com
		  allert@cla-it.it
		  allert@puma5.biz
		  allert@puma5.com
		  allert@puma5.eu
		  allert@puma5.info
		  allert@puma5.it
		  notification@cla-it.com
		  notification@cla-it.it
		  notification@puma5.biz
		  notification@puma5.com
		  notification@puma5.eu
		  notification@puma5.info
		  notification@puma5.it
		  allert@cl2001.it
		  alert@cl2001.it
		  notification@cl2001.it
		 name: allert@cla-it.eu / notification
		 adminCreated: True
		 directMembersCount: 3
		 email: allert@cla-it.eu
		 aliases:
		  alert@cla-it.eu
		  notification@cla-it.eu
		 id: 00rjefff2qrme4v
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: andrea.copelli@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		Total 3 users in group
	amministrazione01@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  amministrazione01@cla-it.it
		  amministrazione01@puma5.com
		  amministrazione01@puma5.eu
		  amministrazione01@puma5.info
		  amministrazione01@puma5.biz
		  amministrazione01@puma5.it
		  amministrazione01@cla-it.com
		  amministrazione01@cl2001.it
		 name: amministrazione01@cl2001.it
		 adminCreated: True
		 directMembersCount: 6
		 email: amministrazione01@cla-it.eu
		 id: 035nkun23o9r0g5
		 description: Amministrazione CL2001
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: eliana.franchi@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: tina.germino@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 6 users in group
	amministrazione@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  amministrazione@cla-it.it
		  amministrazione@puma5.com
		  amministrazione@puma5.eu
		  amministrazione@puma5.info
		  amministrazione@puma5.biz
		  amministrazione@puma5.it
		  amministrazione@cla-it.com
		  amministrazione@cl2001.it
		 name: amministrazione@cla-it.eu
		 adminCreated: True
		 directMembersCount: 7
		 email: amministrazione@cla-it.eu
		 id: 01y810tw1eld6te
		 description: Amministrazione CLA
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 primaryLanguage: en
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: true
		 allowWebPosting: false
		 sendMessageDenyNotification: false
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 messageModerationLevel: MODERATE_NONE
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: ALLOW
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: eliana.franchi@cla-it.eu (user)
		 owner: federico.bulla@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: tina.germino@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 7 users in group
	assenze@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  assenze@cla-it.it
		  assenze@puma5.com
		  assenze@puma5.eu
		  assenze@puma5.info
		  assenze@puma5.biz
		  assenze@puma5.it
		  assenze@cla-it.com
		  assenze@cl2001.it
		 name: assenze@cla-it.eu
		 adminCreated: True
		 directMembersCount: 22
		 email: assenze@cla-it.eu
		 id: 02lwamvv0k973m0
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_IN_DOMAIN_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ALL_IN_DOMAIN_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: C00oisewt (customer)
		 member: alessandro.garberi@cla-it.eu (user)
		 member: andrea.copelli@cl2001.it (user)
		 member: andrea.repetti@cl2001.it (user)
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: eliana.franchi@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: gabriele.repetti@cla-it.eu (user)
		 member: giacomo.barbieri@cla-it.eu (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		 member: giuseppe.siboni@cla-it.eu (user)
		 member: jonathan.amodeo@cl2001.it (user)
		 member: laura.figliossi@cl2001.it (user)
		 member: maddalena.losi@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: oscar.bergonzi@cla-it.eu (user)
		 member: paolo.porcari@cl2001.it (user)
		 member: riccardo.maggiore@cla-it.eu (user)
		 member: stefano.cavanna@cl2001.it (user)
		 member: tina.germino@cla-it.eu (user)
		 member: tommaso.sala@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 22 users in group
	australia@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  australia@cla-it.it
		  australia@puma5.com
		  australia@puma5.eu
		  australia@puma5.info
		  australia@puma5.biz
		  australia@puma5.it
		  australia@cla-it.com
		  australia@cl2001.it
		 name: australia@cla-it.eu
		 adminCreated: True
		 directMembersCount: 3
		 email: australia@cla-it.eu
		 id: 01mrcu092li41e9
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: dave.crosby@deltasol.com.au (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		Total 3 users in group
	authentication@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  Authentication@cla-it.it
		  Authentication@puma5.com
		  Authentication@puma5.eu
		  Authentication@puma5.info
		  Authentication@puma5.biz
		  Authentication@puma5.it
		  Authentication@cla-it.com
		  youtrack@cla-it.com
		  youtrack@cla-it.it
		  youtrack@puma5.biz
		  youtrack@puma5.com
		  youtrack@puma5.eu
		  youtrack@puma5.info
		  youtrack@puma5.it
		  youtrack@cl2001.it
		  Authentication@cl2001.it
		 name: authentication@cla-it.eu / youtrack
		 adminCreated: True
		 directMembersCount: 4
		 email: authentication@cla-it.eu
		 aliases:
		  youtrack@cla-it.eu
		 id: 03dy6vkm3gc4z7r
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: ALL_IN_DOMAIN_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MEMBERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 owner: alessandro.garberi@cla-it.eu (user)
		 owner: federico.bulla@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		Total 4 users in group
	bank.beep@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  bank.beep@cla-it.it
		  bank.beep@puma5.com
		  bank.beep@puma5.eu
		  bank.beep@puma5.info
		  bank.beep@puma5.biz
		  bank.beep@puma5.it
		  bank.beep@cla-it.com
		  bank.beep@cl2001.it
		 name: bank.beep@cla-it.eu
		 adminCreated: True
		 directMembersCount: 2
		 email: bank.beep@cla-it.eu
		 id: 00nmf14n21bez7v
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 owner: federico.bulla@cla-it.eu (user)
		 member: tina.germino@cla-it.eu (user)
		Total 2 users in group
	cla4eni@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  cla4eni@cla-it.it
		  cla4eni@puma5.com
		  cla4eni@puma5.eu
		  cla4eni@puma5.info
		  cla4eni@puma5.biz
		  cla4eni@puma5.it
		  cla4eni@cla-it.com
		  cla4eni@cl2001.it
		 name: cla4eni@cla-it.eu
		 adminCreated: True
		 directMembersCount: 5
		 email: cla4eni@cla-it.eu
		 id: 02250f4o3vun0jl
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: daniela.nitu@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: oscar.bergonzi@cla-it.eu (user)
		 member: riccardo.maggiore@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 5 users in group
	claro@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  claro@cla-it.it
		  claro@puma5.com
		  claro@puma5.eu
		  claro@puma5.info
		  claro@puma5.biz
		  claro@puma5.it
		  claro@cla-it.com
		  claro@cl2001.it
		 name: claro
		 adminCreated: True
		 directMembersCount: 2
		 email: claro@cla-it.eu
		 id: 01fob9te3ps1n61
		 description: Per casella vocale Anna e Daniela
		 allowExternalMembers: false
		 whoCanJoin: ALL_IN_DOMAIN_CAN_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ALL_IN_DOMAIN_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: ana.dumitrescu@cla-it.eu (user)
		 member: daniela.nitu@cla-it.eu (user)
		Total 2 users in group
	confluence-team@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  confluence-team@cla-it.it
		  confluence-team@cl2001.it
		  confluence-team@cla-it.com
		  confluence-team@puma5.biz
		  confluence-team@puma5.com
		  confluence-team@puma5.eu
		  confluence-team@puma5.info
		  confluence-team@puma5.it
		 name: confluence-team
		 adminCreated: True
		 directMembersCount: 5
		 email: confluence-team@cla-it.eu
		 id: 03j2qqm31kc3anb
		 description: Administrators Atlassian Confluence
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ALL_IN_DOMAIN_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: andrea.repetti@cla-it.eu (user)
		 member: federico.bulla@cla-it.eu (user)
		 manager: giuseppe.schifitto@cla-it.eu (user)
		 member: paolo.porcari@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 5 users in group
	convention@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  convention@cla-it.it
		  convention@puma5.com
		  convention@puma5.eu
		  convention@puma5.info
		  convention@puma5.biz
		  convention@puma5.it
		  convention@cla-it.com
		  convention@cl2001.it
		 name: convention@cla-it.eu
		 adminCreated: True
		 directMembersCount: 2
		 email: convention@cla-it.eu
		 id: 00ihv6363q58rpa
		 description: Mailing list per gli inviti alla convention
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 primaryLanguage: en
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 sendMessageDenyNotification: false
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 messageModerationLevel: MODERATE_NONE
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: true
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: true
		Members:
		 owner: beatrice.milanesi@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		Total 2 users in group
	cosmo.development@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  cosmo.development@cla-it.it
		  cosmo.development@puma5.com
		  cosmo.development@puma5.eu
		  cosmo.development@puma5.info
		  cosmo.development@puma5.biz
		  cosmo.development@puma5.it
		  cosmo.development@cla-it.com
		  cosmo.development@cl2001.it
		 name: cosmo.development@cla-it.eu
		 adminCreated: True
		 directMembersCount: 5
		 email: cosmo.development@cla-it.eu
		 id: 02y3w2472bvrskn
		 description: Mailing List per lo sviluppo di Cosmo
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 primaryLanguage: it
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 sendMessageDenyNotification: false
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 messageModerationLevel: MODERATE_NONE
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: FIXED_WIDTH_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: gabriele.repetti@cla-it.eu (user)
		 member: giacomo.barbieri@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: oscar.bergonzi@cla-it.eu (user)
		 member: patrick.nani@cla-it.eu (user)
		Total 5 users in group
	customer.service@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  customer.service@cla-it.it
		  customer.service@puma5.com
		  customer.service@puma5.eu
		  customer.service@puma5.info
		  customer.service@puma5.biz
		  customer.service@puma5.it
		  customer.service@cla-it.com
		  customer.service@cl2001.it
		 name: customer.service@cla-it.eu
		 adminCreated: True
		 directMembersCount: 6
		 email: customer.service@cla-it.eu
		 id: 03dy6vkm3epgijn
		 description: Account comune per il supporto Parature
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: gabriele.repetti@cla-it.eu (user)
		 member: giacomo.barbieri@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: maddalena.losi@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: tommaso.sala@cla-it.eu (user)
		Total 6 users in group
		development@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  development@cla-it.it
		  development@puma5.com
		  development@puma5.eu
		  development@puma5.info
		  development@puma5.biz
		  development@puma5.it
		  development@cla-it.com
		  development@cl2001.it
		 name: development@cla-it.eu
		 adminCreated: True
		 directMembersCount: 10
		 email: development@cla-it.eu
		 id: 04f1mdlm1v41bax
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: alessandro.garberi@cla-it.eu (user)
		 member: gabriele.repetti@cla-it.eu (user)
		 member: giacomo.barbieri@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: giuseppe.siboni@cla-it.eu (user)
		 member: jonathan.amodeo@cla-it.eu (user)
		 member: maddalena.losi@cla-it.eu (user)
		 member: patrick.nani@cla-it.eu (user)
		 member: tommaso.sala@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 10 users in group
	efax@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  efax@cla-it.it
		  efax@puma5.com
		  efax@puma5.eu
		  efax@puma5.info
		  efax@puma5.biz
		  efax@puma5.it
		  efax@cla-it.com
		  efax@cl2001.it
		 name: efax@cla-it.eu
		 adminCreated: True
		 directMembersCount: 5
		 email: efax@cla-it.eu
		 id: 03hv69ve1t9p46j
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 primaryLanguage: it
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 sendMessageDenyNotification: false
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 messageModerationLevel: MODERATE_NONE
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: ALLOW
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: andrea.copelli@cl2001.it (user)
		 member: beatrice.milanesi@cla-it.eu (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: tina.germino@cla-it.eu (user)
		Total 5 users in group
	efaxcl2001@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  efaxcl2001@cla-it.it
		  efaxcl2001@puma5.com
		  efaxcl2001@puma5.eu
		  efaxcl2001@puma5.info
		  efaxcl2001@puma5.biz
		  efaxcl2001@puma5.it
		  efaxcl2001@cla-it.com
		  efaxcl2001@cl2001.it
		 name: efaxcl2001@cl2001.it
		 adminCreated: True
		 directMembersCount: 7
		 email: efaxcl2001@cla-it.eu
		 id: 02fk6b3p24awq2u
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_IN_DOMAIN_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: C00oisewt (customer)
		 member: andrea.copelli@cl2001.it (user)
		 member: eliana.franchi@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: jonathan.amodeo@cl2001.it (user)
		 member: laura.figliossi@cl2001.it (user)
		 member: paolo.porcari@cl2001.it (user)
		Total 7 users in group
	employees@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  employees@cla-it.it
		  employees@puma5.com
		  employees@puma5.eu
		  employees@puma5.info
		  employees@puma5.biz
		  employees@puma5.it
		  employees@cla-it.com
		  employees@cl2001.it
		 name: employees@cla-it.eu / cl2001.it
		 adminCreated: True
		 directMembersCount: 1
		 email: employees@cla-it.eu
		 id: 01y810tw1k68bbg
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_IN_DOMAIN_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ALL_MEMBERS_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: C00oisewt (customer)
		Total 1 users in group
	farmsupport@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  farmsupport@cla-it.it
		  farmsupport@puma5.com
		  farmsupport@puma5.eu
		  farmsupport@puma5.info
		  farmsupport@puma5.biz
		  farmsupport@puma5.it
		  farmsupport@cla-it.com
		  farmsupport@cl2001.it
		  farm-support@cl2001.it
		  farm-support@cla-it.com
		  farm-support@cla-it.it
		  farm-support@puma5.biz
		  farm-support@puma5.com
		  farm-support@puma5.eu
		  farm-support@puma5.info
		  farm-support@puma5.it
		 name: farmsupport@cla-it.eu
		 adminCreated: True
		 directMembersCount: 2
		 email: farmsupport@cla-it.eu
		 aliases:
		  farm-support@cla-it.eu
		 id: 04h042r025l2jof
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_IN_DOMAIN_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MANAGERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 owner: federico.bulla@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		Total 2 users in group
	hsupport@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  hsupport@cla-it.it
		  hsupport@puma5.com
		  hsupport@puma5.eu
		  hsupport@puma5.info
		  hsupport@puma5.biz
		  hsupport@puma5.it
		  hsupport@cla-it.com
		  ala@cla-it.com
		  ala@cla-it.it
		  ala@puma5.biz
		  ala@puma5.com
		  ala@puma5.eu
		  ala@puma5.info
		  ala@puma5.it
		  gamma@cla-it.com
		  gamma@cla-it.it
		  gamma@puma5.biz
		  gamma@puma5.com
		  gamma@puma5.eu
		  gamma@puma5.info
		  gamma@puma5.it
		  fugas@cla-it.com
		  fugas@cla-it.it
		  fugas@puma5.biz
		  fugas@puma5.com
		  fugas@puma5.eu
		  fugas@puma5.info
		  fugas@puma5.it
		  sini@cla-it.com
		  sini@cla-it.it
		  sini@puma5.biz
		  sini@puma5.com
		  sini@puma5.eu
		  sini@puma5.info
		  sini@puma5.it
		  hsupport@cl2001.it
		  ala@cl2001.it
		  fugas@cl2001.it
		  gamma@cl2001.it
		  sini@cl2001.it
		  ipam@cl2001.it
		  ipam@cla-it.com
		  ipam@cla-it.it
		  ipam@puma5.biz
		  ipam@puma5.com
		  ipam@puma5.eu
		  ipam@puma5.info
		  ipam@puma5.it
		 name: hsupport@cla-it.eu
		 adminCreated: True
		 directMembersCount: 7
		 email: hsupport@cla-it.eu
		 aliases:
		  ala@cla-it.eu
		  gamma@cla-it.eu
		  fugas@cla-it.eu
		  sini@cla-it.eu
		  ipam@cla-it.eu
		 id: 02iq8gzs3o4zaiv
		 description: Supporto hardware clienti rilevanti
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: andrea.copelli@cl2001.it (user)
		 member: andrea.repetti@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		 member: laura.figliossi@cl2001.it (user)
		 member: paolo.porcari@cl2001.it (user)
		 member: stefano.cavanna@cl2001.it (user)
		Total 7 users in group
	incl2001@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  incl2001@cla-it.it
		  incl2001@puma5.com
		  incl2001@puma5.eu
		  incl2001@puma5.info
		  incl2001@puma5.biz
		  incl2001@puma5.it
		  incl2001@cla-it.com
		  incl2001@cl2001.it
		 name: incl2001@cl2001.it
		 adminCreated: True
		 directMembersCount: 11
		 email: incl2001@cla-it.eu
		 id: 02jxsxqh0i8rwm1
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_IN_DOMAIN_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: andrea.copelli@cl2001.it (user)
		 member: andrea.repetti@cl2001.it (user)
		 member: eliana.franchi@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		 member: jonathan.amodeo@cl2001.it (user)
		 member: laura.figliossi@cl2001.it (user)
		 member: paolo.porcari@cl2001.it (user)
		 member: stefano.cavanna@cl2001.it (user)
		 member: tina.germino@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 11 users in group
	incla@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  incla@cla-it.it
		  incla@puma5.com
		  incla@puma5.eu
		  incla@puma5.info
		  incla@puma5.biz
		  incla@puma5.it
		  incla@cla-it.com
		  incla@cl2001.it
		 name: incla@cla-it.eu
		 adminCreated: True
		 directMembersCount: 16
		 email: incla@cla-it.eu
		 id: 026in1rg2i7of3j
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ALL_IN_DOMAIN_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: alessandro.garberi@cla-it.eu (user)
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: daniela.nitu@cla-it.eu (user)
		 member: federico.bulla@cla-it.eu (user)
		 member: gabriele.repetti@cla-it.eu (user)
		 member: giacomo.barbieri@cla-it.eu (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		 member: giuseppe.siboni@cla-it.eu (user)
		 member: maddalena.losi@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: oscar.bergonzi@cla-it.eu (user)
		 member: riccardo.maggiore@cla-it.eu (user)
		 member: tina.germino@cla-it.eu (user)
		 member: tommaso.sala@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 16 users in group
	india@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  india@cla-it.it
		  india@puma5.com
		  india@puma5.eu
		  india@puma5.info
		  india@puma5.biz
		  india@puma5.it
		  india@cla-it.com
		  india@cl2001.it
		 name: india@cla-it.eu
		 adminCreated: True
		 directMembersCount: 3
		 email: india@cla-it.eu
		 id: 00z337ya4g4qndi
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 3 users in group
	info@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  info@cla-it.it
		  info@puma5.com
		  info@puma5.eu
		  info@puma5.info
		  info@puma5.biz
		  info@puma5.it
		  info@cla-it.com
		  infopuma5@cla-it.com
		  infopuma5@cla-it.it
		  infopuma5@puma5.biz
		  infopuma5@puma5.com
		  infopuma5@puma5.eu
		  infopuma5@puma5.info
		  infopuma5@puma5.it
		  puma5info@cla-it.com
		  puma5info@cla-it.it
		  puma5info@puma5.biz
		  puma5info@puma5.com
		  puma5info@puma5.eu
		  puma5info@puma5.info
		  puma5info@puma5.it
		  info@cl2001.it
		  infopuma5@cl2001.it
		  puma5info@cl2001.it
		 name: Info@cla-it.eu / cl2001.it
		 adminCreated: True
		 directMembersCount: 9
		 email: info@cla-it.eu
		 aliases:
		  infopuma5@cla-it.eu
		  puma5info@cla-it.eu
		 id: 017dp8vu3oyam9w
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 primaryLanguage: en
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: Your message to the Info group has been rejected, please stop the SPAM on this address
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 sendMessageDenyNotification: true
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 messageModerationLevel: MODERATE_ALL_MESSAGES
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: FIXED_WIDTH_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: SILENTLY_MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: true
		Members:
		 member: andrea.copelli@cl2001.it (user)
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: eliana.franchi@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: laura.figliossi@cl2001.it (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: tina.germino@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 9 users in group
	infoprivacy@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  infoprivacy@cla-it.it
		  infoprivacy@cl2001.it
		  infoprivacy@cla-it.com
		  infoprivacy@puma5.biz
		  infoprivacy@puma5.com
		  infoprivacy@puma5.eu
		  infoprivacy@puma5.info
		  infoprivacy@puma5.it
		 name: infoprivacy
		 adminCreated: True
		 directMembersCount: 2
		 email: infoprivacy@cla-it.eu
		 id: 00upglbi1i2ewmd
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: eliana.franchi@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		Total 2 users in group
	iran@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  iran@cla-it.it
		  iran@puma5.com
		  iran@puma5.eu
		  iran@puma5.info
		  iran@puma5.biz
		  iran@puma5.it
		  iran@cla-it.com
		  iran@cl2001.it
		 name: iran@cla-it.eu
		 adminCreated: True
		 directMembersCount: 3
		 email: iran@cla-it.eu
		 id: 036ei31r2y68r2w
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: pbehzad@samyaco.com (user)
		Total 3 users in group
	jira-dev-team@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  jira-dev-team@puma5.info
		  jira-dev-team@cl2001.it
		  jira-dev-team@cla-it.com
		  jira-dev-team@cla-it.it
		  jira-dev-team@puma5.biz
		  jira-dev-team@puma5.com
		  jira-dev-team@puma5.eu
		  jira-dev-team@puma5.it
		 name: jira-dev-team
		 adminCreated: True
		 directMembersCount: 3
		 email: jira-dev-team@cla-it.eu
		 id: 01y810tw43l0h24
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ALL_IN_DOMAIN_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: alessandro.garberi@cla-it.eu (user)
		 member: gabriele.repetti@cla-it.eu (user)
		 member: giuseppe.siboni@cla-it.eu (user)
		Total 3 users in group
	jobs@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  jobs@cla-it.it
		  jobs@puma5.com
		  jobs@puma5.eu
		  jobs@puma5.info
		  jobs@puma5.biz
		  jobs@puma5.it
		  jobs@cla-it.com
		  jobs@cl2001.it
		 name: jobs@cla-it.eu
		 adminCreated: True
		 directMembersCount: 4
		 email: jobs@cla-it.eu
		 id: 02u6wntf3vg83b3
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 primaryLanguage: en
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 sendMessageDenyNotification: false
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 messageModerationLevel: MODERATE_NONE
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: true
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: alessandro.garberi@cla-it.eu (user)
		 owner: federico.bulla@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 4 users in group
	lansupport@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  lansupport@cla-it.it
		  lansupport@puma5.com
		  lansupport@puma5.eu
		  lansupport@puma5.info
		  lansupport@puma5.biz
		  lansupport@puma5.it
		  lansupport@cla-it.com
		  mcafeemsemea@cla-it.com
		  mcafeemsemea@cla-it.it
		  mcafeemsemea@puma5.biz
		  mcafeemsemea@puma5.com
		  mcafeemsemea@puma5.eu
		  mcafeemsemea@puma5.info
		  mcafeemsemea@puma5.it
		  soft@cla-it.com
		  soft@cla-it.it
		  soft@puma5.biz
		  soft@puma5.com
		  soft@puma5.eu
		  soft@puma5.info
		  soft@puma5.it
		  lansupport@cl2001.it
		  mcafeemsemea@cl2001.it
		  soft@cl2001.it
		 name: lansupport@cla-it.eu / cl2001.it
		 adminCreated: True
		 directMembersCount: 6
		 email: lansupport@cla-it.eu
		 aliases:
		  mcafeemsemea@cla-it.eu
		  soft@cla-it.eu
		 id: 048pi1tg2ll9mgj
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: true
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: andrea.copelli@cl2001.it (user)
		 member: andrea.repetti@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		 member: paolo.porcari@cl2001.it (user)
		 member: stefano.cavanna@cl2001.it (user)
		Total 6 users in group
	legal@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  legal@cla-it.it
		  legal@puma5.com
		  legal@puma5.eu
		  legal@puma5.info
		  legal@puma5.biz
		  legal@puma5.it
		  legal@cla-it.com
		  legal@cl2001.it
		 name: legal@cla-it.eu
		 adminCreated: True
		 directMembersCount: 2
		 email: legal@cla-it.eu
		 id: 00z337ya31yria0
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		Total 2 users in group
	marketing@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  marketing@cla-it.it
		  marketing@puma5.com
		  marketing@puma5.eu
		  marketing@puma5.info
		  marketing@puma5.biz
		  marketing@puma5.it
		  marketing@cla-it.com
		  marketing@cl2001.it
		 name: marketing@cla-it.eu
		 adminCreated: True
		 directMembersCount: 3
		 email: marketing@cla-it.eu
		 id: 0111kx3o2t9dmmu
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		Total 3 users in group
	netherlands@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  netherlands@cla-it.it
		  netherlands@puma5.com
		  netherlands@puma5.eu
		  netherlands@puma5.info
		  netherlands@puma5.biz
		  netherlands@puma5.it
		  netherlands@cla-it.com
		  netherlands@cl2001.it
		 name: netherlands@cla-it.eu
		 adminCreated: True
		 directMembersCount: 2
		 email: netherlands@cla-it.eu
		 id: 03tbugp13n8s6c6
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		Total 2 users in group
	no-reply@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  no-reply@cla-it.it
		  no-reply@puma5.com
		  no-reply@puma5.eu
		  no-reply@puma5.info
		  no-reply@puma5.biz
		  no-reply@puma5.it
		  no-reply@cla-it.com
		  no-reply@cl2001.it
		 name: no-reply@cla-it.eu
		 adminCreated: True
		 directMembersCount: 1
		 email: no-reply@cla-it.eu
		 id: 037m2jsg3dwmvmt
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ALL_OWNERS_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_OWNERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		Total 1 users in group
	presenze@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  presenze@cla-it.it
		  presenze@puma5.com
		  presenze@puma5.eu
		  presenze@puma5.info
		  presenze@puma5.biz
		  presenze@puma5.it
		  presenze@cla-it.com
		  presenze@cl2001.it
		 name: presenze@cla-it.eu
		 adminCreated: True
		 directMembersCount: 4
		 email: presenze@cla-it.eu
		 id: 03l18frh0u70hrk
		 description: Gestione Problematiche rilevazione presenze personale
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_MEMBERS_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ALL_IN_DOMAIN_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: federico.bulla@cla-it.eu (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		 member: tina.germino@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 4 users in group
	providers@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  providers@cla-it.it
		  providers@puma5.com
		  providers@puma5.eu
		  providers@puma5.info
		  providers@puma5.biz
		  providers@puma5.it
		  providers@cla-it.com
		  providers@cl2001.it
		 name: providers@cl2001.it
		 adminCreated: True
		 directMembersCount: 3
		 email: providers@cla-it.eu
		 id: 04k668n33e1isnf
		 description: Gruppo per comunicazioni Tecnico/Commerciali con i fornitori di CL2001
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ALL_IN_DOMAIN_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: andrea.copelli@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: laura.figliossi@cl2001.it (user)
		Total 3 users in group
	sales@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  sales@cla-it.it
		  sales@puma5.com
		  sales@puma5.eu
		  sales@puma5.info
		  sales@puma5.biz
		  sales@puma5.it
		  sales@cla-it.com
		  sales@cl2001.it
		 name: sales@cla-it.eu
		 adminCreated: True
		 directMembersCount: 5
		 email: sales@cla-it.eu
		 id: 01qoc8b13e6qeck
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: Your message to the Info group has been rejected, please stop the SPAM on this address
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_ALL_MESSAGES
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: true
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_OWNERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: federico.bulla@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 5 users in group
		services@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  services@cla-it.it
		  services@puma5.com
		  services@puma5.eu
		  services@puma5.info
		  services@puma5.biz
		  services@puma5.it
		  services@cla-it.com
		  services@cl2001.it
		 name: services@cla-it.eu
		 adminCreated: True
		 directMembersCount: 7
		 email: services@cla-it.eu
		 id: 03s49zyc42kl9at
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: maddalena.losi@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: oscar.bergonzi@cla-it.eu (user)
		 member: patrick.nani@cla-it.eu (user)
		 member: riccardo.maggiore@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 7 users in group
	singapore@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  singapore@cla-it.it
		  singapore@puma5.com
		  singapore@puma5.eu
		  singapore@puma5.info
		  singapore@puma5.biz
		  singapore@puma5.it
		  singapore@cla-it.com
		  singapore@cl2001.it
		 name: singapore@cla-it.eu
		 adminCreated: True
		 directMembersCount: 4
		 email: singapore@cla-it.eu
		 id: 02w5ecyt2j7v51f
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: chris@teamdesignsolutions.com (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		 member: gts@teamdesignsolutions.com (user)
		Total 4 users in group
	software@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  software@cla-it.it
		  software@puma5.com
		  software@puma5.eu
		  software@puma5.info
		  software@puma5.biz
		  software@puma5.it
		  software@cla-it.com
		  software@cl2001.it
		 name: software@cl2001.it
		 adminCreated: True
		 directMembersCount: 9
		 email: software@cla-it.eu
		 id: 02grqrue2dgu1qh
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: true
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MANAGERS_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: alessandro.garberi@cla-it.eu (user)
		 member: andrea.copelli@cl2001.it (user)
		 member: andrea.repetti@cl2001.it (user)
		 member: eliana.franchi@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		 member: jonathan.amodeo@cl2001.it (user)
		 member: paolo.porcari@cl2001.it (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 9 users in group
	southafrica@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  southafrica@cla-it.it
		  southafrica@puma5.com
		  southafrica@puma5.eu
		  southafrica@puma5.info
		  southafrica@puma5.biz
		  southafrica@puma5.it
		  southafrica@cla-it.com
		  southafrica@cl2001.it
		 name: southafrica@cla-it.eu
		 adminCreated: True
		 directMembersCount: 2
		 email: southafrica@cla-it.eu
		 id: 02et92p03hxzrm8
		 description: 
		 allowExternalMembers: true
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		Total 2 users in group
	southkorea@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  southkorea@cla-it.it
		  southkorea@puma5.com
		  southkorea@puma5.eu
		  southkorea@puma5.info
		  southkorea@puma5.biz
		  southkorea@puma5.it
		  southkorea@cla-it.com
		  southkorea@cl2001.it
		 name: southkorea@cla-it.eu
		 adminCreated: True
		 directMembersCount: 3
		 email: southkorea@cla-it.eu
		 id: 03l18frh1prydxk
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: beatrice.milanesi@cla-it.eu (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		 member: steve.kim@uitsolutions.com (user)
		Total 3 users in group
	spcpuma.baltim@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.baltim@puma5.info
		  spcpuma.baltim@cl2001.it
		  spcpuma.baltim@cla-it.com
		  spcpuma.baltim@cla-it.it
		  spcpuma.baltim@puma5.biz
		  spcpuma.baltim@puma5.com
		  spcpuma.baltim@puma5.eu
		  spcpuma.baltim@puma5.it
		 name: SPCPuma Baltm
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.baltim@cla-it.eu
		 id: 034g0dwd13ttk8c
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.baltim@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.cafc@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.cafc@cla-it.it
		  spcpuma.cafc@puma5.com
		  spcpuma.cafc@puma5.eu
		  spcpuma.cafc@puma5.info
		  spcpuma.cafc@puma5.biz
		  spcpuma.cafc@puma5.it
		  spcpuma.cafc@cla-it.com
		  spcpuma.cafc@cl2001.it
		 name: spcpuma.cafc@cla-it.eu
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.cafc@cla-it.eu
		 id: 00xvir7l2k1doz3
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.cafc@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.cec@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.cec@puma5.eu
		  spcpuma.cec@cl2001.it
		  spcpuma.cec@cla-it.com
		  spcpuma.cec@cla-it.it
		  spcpuma.cec@puma5.biz
		  spcpuma.cec@puma5.com
		  spcpuma.cec@puma5.info
		  spcpuma.cec@puma5.it
		 name: SPC Puma CEC
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.cec@cla-it.eu
		 id: 01ksv4uv4fj412n
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.cec@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.codes@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.codifica@cla-it.it
		  spcpuma.codifica@puma5.com
		  spcpuma.codifica@puma5.eu
		  spcpuma.codifica@puma5.info
		  spcpuma.codifica@puma5.biz
		  spcpuma.codifica@puma5.it
		  spcpuma.codifica@cla-it.com
		  spcpuma.codes@cla-it.com
		  spcpuma.codes@cla-it.it
		  spcpuma.codes@puma5.biz
		  spcpuma.codes@puma5.com
		  spcpuma.codes@puma5.eu
		  spcpuma.codes@puma5.info
		  spcpuma.codes@puma5.it
		  spcpuma.codes@cl2001.it
		  spcpuma.codifica@cl2001.it
		 name: Support SPC Puma Codifica
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.codes@cla-it.eu
		 aliases:
		  spcpuma.codifica@cla-it.eu
		 id: 03j2qqm32y0xbfx
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.codes@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.emims@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.emims@cla-it.it
		  spcpuma.emims@puma5.com
		  spcpuma.emims@puma5.eu
		  spcpuma.emims@puma5.info
		  spcpuma.emims@puma5.biz
		  spcpuma.emims@puma5.it
		  spcpuma.emims@cla-it.com
		  spcpuma.emims@cl2001.it
		 name: SPC Puma vs EMIMS (Integrazione)
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.emims@cla-it.eu
		 id: 045jfvxd2zrk3ld
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: ALL_IN_DOMAIN_CAN_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.emims@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.mozambique@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.mozambique@cla-it.it
		  spcpuma.mozambique@puma5.com
		  spcpuma.mozambique@puma5.eu
		  spcpuma.mozambique@puma5.info
		  spcpuma.mozambique@puma5.biz
		  spcpuma.mozambique@puma5.it
		  spcpuma.mozambique@cla-it.com
		  spcpuma.mozambique@cl2001.it
		 name: SPCPuma Mozambique
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.mozambique@cla-it.eu
		 id: 01302m923dre0wv
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.mozambique@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.okpai@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.okpai@cla-it.it
		  spcpuma.okpai@cl2001.it
		  spcpuma.okpai@cla-it.com
		  spcpuma.okpai@puma5.biz
		  spcpuma.okpai@puma5.com
		  spcpuma.okpai@puma5.eu
		  spcpuma.okpai@puma5.info
		  spcpuma.okpai@puma5.it
		 name: SPCPuma OKPAI
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.okpai@cla-it.eu
		 id: 01yyy98l0n2egpo
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.okpai@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.orf@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.orf@puma5.biz
		  spcpuma.orf@cl2001.it
		  spcpuma.orf@cla-it.com
		  spcpuma.orf@cla-it.it
		  spcpuma.orf@puma5.com
		  spcpuma.orf@puma5.eu
		  spcpuma.orf@puma5.info
		  spcpuma.orf@puma5.it
		 name: SPCPuma ORF EPC
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.orf@cla-it.eu
		 id: 03fwokq04ajrj93
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.orf@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.zohr.mru@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.zohr.mru@cla-it.it
		  spcpuma.zohr.mru@puma5.com
		  spcpuma.zohr.mru@puma5.eu
		  spcpuma.zohr.mru@puma5.info
		  spcpuma.zohr.mru@puma5.biz
		  spcpuma.zohr.mru@puma5.it
		  spcpuma.zohr.mru@cla-it.com
		  spcpuma.zohr.mru@cl2001.it
		 name: SPCPuma ZOHR MRU (COMART)
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.zohr.mru@cla-it.eu
		 id: 03mzq4wv4gltzmt
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.zohr.mru@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.zohr.onshore@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.zohr.onshore@cla-it.it
		  spcpuma.zohr.onshore@puma5.com
		  spcpuma.zohr.onshore@puma5.eu
		  spcpuma.zohr.onshore@puma5.info
		  spcpuma.zohr.onshore@puma5.biz
		  spcpuma.zohr.onshore@puma5.it
		  spcpuma.zohr.onshore@cla-it.com
		  spcpuma.zohr.onshore@cl2001.it
		 name: SPCPuma ZOHR OnShore
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.zohr.onshore@cla-it.eu
		 id: 02szc72q1sacwup
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: ALL_IN_DOMAIN_CAN_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.zohr.onshore@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.zohr.sru@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.zohr.sru@cla-it.it
		  spcpuma.zohr.sru@puma5.com
		  spcpuma.zohr.sru@puma5.eu
		  spcpuma.zohr.sru@puma5.info
		  spcpuma.zohr.sru@puma5.biz
		  spcpuma.zohr.sru@puma5.it
		  spcpuma.zohr.sru@cla-it.com
		  spcpuma.zohr.sru@cl2001.it
		 name: SPCPuma ZOHR SRU (KT)
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.zohr.sru@cla-it.eu
		 id: 045jfvxd2vvfbgt
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.zohr.sru@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma.zohr@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma.zohr@cla-it.it
		  spcpuma.zohr@puma5.com
		  spcpuma.zohr@puma5.eu
		  spcpuma.zohr@puma5.info
		  spcpuma.zohr@puma5.biz
		  spcpuma.zohr@puma5.it
		  spcpuma.zohr@cla-it.com
		  spcpuma.zohr@cl2001.it
		 name: SPCPuma ZOHR
		 adminCreated: True
		 directMembersCount: 1
		 email: spcpuma.zohr@cla-it.eu
		 id: 00rjefff3lnmvnd
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: ALL_IN_DOMAIN_CAN_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma.zohr@cla.freshdesk.com (user)
		Total 1 users in group
	spcpuma@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  spcpuma@cla-it.it
		  spcpuma@puma5.com
		  spcpuma@puma5.eu
		  spcpuma@puma5.info
		  spcpuma@puma5.biz
		  spcpuma@puma5.it
		  spcpuma@cla-it.com
		  spcpuma@cl2001.it
		 name: Support SPC Puma
		 adminCreated: True
		 directMembersCount: 3
		 email: spcpuma@cla-it.eu
		 id: 00zu0gcz3uhm0r0
		 description: Request help on SPC Puma
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: ALLOW
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteuspcpuma@cla.freshdesk.com (user)
		 member: marcello.poggi@eni.com (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 3 users in group
	support.arco@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.arco@cla-it.it
		  support.arco@puma5.com
		  support.arco@puma5.eu
		  support.arco@puma5.info
		  support.arco@puma5.biz
		  support.arco@puma5.it
		  support.arco@cla-it.com
		  support.arco@cl2001.it
		 name: Supporto Arco
		 adminCreated: True
		 directMembersCount: 1
		 email: support.arco@cla-it.eu
		 id: 04du1wux1i93z44
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cl2001itsupport.arco@cla.freshdesk.com (user)
		Total 1 users in group
	support.bidman@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.bidman@cla-it.it
		  support.bidman@puma5.com
		  support.bidman@puma5.eu
		  support.bidman@puma5.info
		  support.bidman@puma5.biz
		  support.bidman@puma5.it
		  support.bidman@cla-it.com
		  support.bidman@cl2001.it
		 name: Support Bidman
		 adminCreated: True
		 directMembersCount: 2
		 email: support.bidman@cla-it.eu
		 id: 02y3w2472g0k8rd
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.bidman@cla.freshdesk.com (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		Total 2 users in group
	support.cloud@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.cloud@cla-it.it
		  support.cloud@puma5.com
		  support.cloud@puma5.eu
		  support.cloud@puma5.info
		  support.cloud@puma5.biz
		  support.cloud@puma5.it
		  support.cloud@cla-it.com
		  support.cloud@cl2001.it
		 name: Support Cloud
		 adminCreated: True
		 directMembersCount: 3
		 email: support.cloud@cla-it.eu
		 id: 04k668n30oj0zip
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.cloud@cla.freshdesk.com (user)
		 member: federico.bulla@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		Total 3 users in group
	support.cosmo@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.cosmo@cla-it.it
		  support.cosmo@puma5.com
		  support.cosmo@puma5.eu
		  support.cosmo@puma5.info
		  support.cosmo@puma5.biz
		  support.cosmo@puma5.it
		  support.cosmo@cla-it.com
		  support.cosmo@cl2001.it
		 name: Support Cosmo
		 adminCreated: True
		 directMembersCount: 2
		 email: support.cosmo@cla-it.eu
		 id: 03as4poj3ydnrnv
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.cosmo@cla.freshdesk.com (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		Total 2 users in group
	support.elena@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.elena@cla-it.it
		  support.elena@puma5.com
		  support.elena@puma5.eu
		  support.elena@puma5.info
		  support.elena@puma5.biz
		  support.elena@puma5.it
		  support.elena@cla-it.com
		  support.elena@cl2001.it
		 name: Support ELENA
		 adminCreated: True
		 directMembersCount: 1
		 email: support.elena@cla-it.eu
		 id: 03hv69ve0jhdw92
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.elena@cla.freshdesk.com (user)
		Total 1 users in group
	support.order@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.order@cla-it.it
		  support.order@puma5.com
		  support.order@puma5.eu
		  support.order@puma5.info
		  support.order@puma5.biz
		  support.order@puma5.it
		  support.order@cla-it.com
		  support.order@cl2001.it
		 name: Support Order
		 adminCreated: True
		 directMembersCount: 1
		 email: support.order@cla-it.eu
		 id: 04du1wux2082zk9
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.order@cla.freshdesk.com (user)
		Total 1 users in group
	support.puma@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.puma@cla-it.it
		  support.puma@puma5.com
		  support.puma@puma5.eu
		  support.puma@puma5.info
		  support.puma@puma5.biz
		  support.puma@puma5.it
		  support.puma@cla-it.com
		  support.puma@cl2001.it
		 name: Support Puma
		 adminCreated: True
		 directMembersCount: 1
		 email: support.puma@cla-it.eu
		 id: 00rjefff1ciw0xa
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.puma@cla.freshdesk.com (user)
		Total 1 users in group
	support.spider@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.spider@cla-it.it
		  support.spider@puma5.com
		  support.spider@puma5.eu
		  support.spider@puma5.info
		  support.spider@puma5.biz
		  support.spider@puma5.it
		  support.spider@cla-it.com
		  support.spider@cl2001.it
		 name: Support Spider
		 adminCreated: True
		 directMembersCount: 1
		 email: support.spider@cla-it.eu
		 id: 00haapch4565vq5
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.spider@cla.freshdesk.com (user)
		Total 1 users in group
	support.sumo@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.sumo@cla-it.it
		  support.sumo@puma5.com
		  support.sumo@puma5.eu
		  support.sumo@puma5.info
		  support.sumo@puma5.biz
		  support.sumo@puma5.it
		  support.sumo@cla-it.com
		  support.sumo@cl2001.it
		 name: Support Sumo
		 adminCreated: True
		 directMembersCount: 1
		 email: support.sumo@cla-it.eu
		 id: 0206ipza4irvxdo
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.sumo@cla.freshdesk.com (user)
		Total 1 users in group
	support.tms@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.tms@cla-it.it
		  support.tms@puma5.com
		  support.tms@puma5.eu
		  support.tms@puma5.info
		  support.tms@puma5.biz
		  support.tms@puma5.it
		  support.tms@cla-it.com
		  support.tms@cl2001.it
		 name: Support TMS
		 adminCreated: True
		 directMembersCount: 2
		 email: support.tms@cla-it.eu
		 id: 04f1mdlm1a8db0v
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.tms@cla.freshdesk.com (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		Total 2 users in group
	support.weldingbook@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support.weldingbook@cla-it.it
		  support.weldingbook@puma5.com
		  support.weldingbook@puma5.eu
		  support.weldingbook@puma5.info
		  support.weldingbook@puma5.biz
		  support.weldingbook@puma5.it
		  support.weldingbook@cla-it.com
		  support.weldingbook@cl2001.it
		 name: Support Welding Book
		 adminCreated: True
		 directMembersCount: 1
		 email: support.weldingbook@cla-it.eu
		 id: 02s8eyo11zxpqfe
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: cla-iteusupport.weldingbook@cla.freshdesk.com (user)
		Total 1 users in group
	support@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  support@cla-it.it
		  support@puma5.com
		  support@puma5.eu
		  support@puma5.info
		  support@puma5.biz
		  support@puma5.it
		  support@cla-it.com
		  support@cl2001.it
		 name: support@cla-it.eu
		 adminCreated: True
		 directMembersCount: 12
		 email: support@cla-it.eu
		 id: 01ci93xb2a67dor
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: Dear User,
		
		this email address is not checked by our support team, if you replied to an email sent from the customer support portal please reply directly from the web site or your message will be lost.
		
		Sincerely
		The CLA support team
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_ALL_MESSAGES
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: true
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: REJECT
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: true
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: eliana.franchi@cla-it.eu (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: gabriele.repetti@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		 member: giuseppe.siboni@cla-it.eu (user)
		 member: maddalena.losi@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: oscar.bergonzi@cla-it.eu (user)
		 member: patrick.nani@cla-it.eu (user)
		 member: tommaso.sala@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 12 users in group
	sw@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  sw@cla-it.it
		  sw@puma5.com
		  sw@puma5.eu
		  sw@puma5.info
		  sw@puma5.biz
		  sw@puma5.it
		  sw@cla-it.com
		  sw@cl2001.it
		 name: sw@cla-it.eu
		 adminCreated: True
		 directMembersCount: 12
		 email: sw@cla-it.eu
		 id: 01opuj5n368zxqo
		 description: Software Notification
		 allowExternalMembers: false
		 whoCanJoin: INVITED_CAN_JOIN
		 whoCanViewMembership: ALL_MEMBERS_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: false
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_MEMBERS_CAN_VIEW
		 showInGroupDirectory: true
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: alessandro.garberi@cla-it.eu (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: gabriele.repetti@cla-it.eu (user)
		 member: giacomo.barbieri@cla-it.eu (user)
		 owner: gianmario.tagliaretti@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		 member: giuseppe.siboni@cla-it.eu (user)
		 member: maddalena.losi@cla-it.eu (user)
		 member: nicoletta.poggi@cla-it.eu (user)
		 member: patrick.nani@cla-it.eu (user)
		 member: tommaso.sala@cla-it.eu (user)
		 member: ubaldo.bulla@cla-it.eu (user)
		Total 12 users in group
	temil@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  temil@cla-it.it
		  temil@puma5.com
		  temil@puma5.eu
		  temil@puma5.info
		  temil@puma5.biz
		  temil@puma5.it
		  temil@cla-it.com
		  temil@cl2001.it
		 name: temil@cla-it.eu
		 adminCreated: True
		 directMembersCount: 3
		 email: temil@cla-it.eu
		 id: 02y3w2472q5tyjw
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MEMBERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 owner: federico.bulla@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		 member: patrick.nani@cla-it.eu (user)
		Total 3 users in group
	temp@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  temp@cla-it.it
		  temp@puma5.com
		  temp@puma5.eu
		  temp@puma5.info
		  temp@puma5.biz
		  temp@puma5.it
		  temp@cla-it.com
		  temp@cl2001.it
		 name: temp@cla-it.eu
		 adminCreated: True
		 directMembersCount: 1
		 email: temp@cla-it.eu
		 id: 03dy6vkm3tc5kqw
		 description: test gruppo per share docs
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: gianmario.tagliaretti@cla-it.eu (user)
		Total 1 users in group
	wf@cla-it.eu
		Group Settings:
		 nonEditableAliases:
		  wf@cla-it.it
		  wf@puma5.com
		  wf@puma5.eu
		  wf@puma5.info
		  wf@puma5.biz
		  wf@puma5.it
		  wf@cla-it.com
		  wf@cl2001.it
		 name: wf@cla-it.eu
		 adminCreated: True
		 directMembersCount: 5
		 email: wf@cla-it.eu
		 id: 03dy6vkm2izl3yx
		 description: 
		 allowExternalMembers: false
		 whoCanJoin: CAN_REQUEST_TO_JOIN
		 whoCanViewMembership: ALL_IN_DOMAIN_CAN_VIEW
		 includeCustomFooter: false
		 defaultMessageDenyNotificationText: 
		 includeInGlobalAddressList: true
		 archiveOnly: false
		 isArchived: true
		 membersCanPostAsTheGroup: false
		 allowWebPosting: true
		 messageModerationLevel: MODERATE_NONE
		 replyTo: REPLY_TO_IGNORE
		 customReplyTo: 
		 sendMessageDenyNotification: false
		 whoCanContactOwner: ANYONE_CAN_CONTACT
		 messageDisplayFont: DEFAULT_FONT
		 whoCanLeaveGroup: ALL_MEMBERS_CAN_LEAVE
		 whoCanAdd: ALL_MANAGERS_CAN_ADD
		 whoCanPostMessage: ANYONE_CAN_POST
		 whoCanInvite: ALL_MANAGERS_CAN_INVITE
		 spamModerationLevel: MODERATE
		 whoCanViewGroup: ALL_IN_DOMAIN_CAN_VIEW
		 showInGroupDirectory: false
		 maxMessageBytes: 25M
		 customFooterText: 
		 allowGoogleCommunication: false
		Members:
		 member: andrea.copelli@cl2001.it (user)
		 owner: federico.bulla@cla-it.eu (user)
		 member: gianmario.tagliaretti@cla-it.eu (user)
		 member: giuseppe.schifitto@cla-it.eu (user)
		 member: tina.germino@cla-it.eu (user)
		Total 5 users in group

#ALIAS

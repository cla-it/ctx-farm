$vcenter = '172.10.250.100'
$victim = 'vm_name'

# Create auth header

$Credential = Get-Credential
$auth = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($Credential.UserName + ':' + $Credential.GetNetworkCredential().Password))
$head = @{
    'Authorization' = "Basic $auth"
}
$auth = $null

# Create a session

$r = Invoke-WebRequest -Uri "https://$vcenter/rest/com/vmware/cis/session" -Method Post -Headers $head
$token = (ConvertFrom-Json $r.Content).value
$session = @{'vmware-api-session-id' = $token}

# Get VMware VM

$r1 = Invoke-WebRequest -Uri "https://$vcenter/rest/vcenter/vm" -Method Get -Headers $session
$vms = (ConvertFrom-Json $r1.Content).value
$vms
$vms.Count

# Find a specific VM

$moref = ($vms | Where-Object {$_.name -eq $victim}).vm
$r2 = Invoke-WebRequest -Uri "https://$vcenter/rest/vcenter/vm/$moref" -Method Get -Headers $session
$thevm = (ConvertFrom-Json $r2.Content).value
$thevm

# Toggle the Power

(Invoke-RestMethod -Uri "https://$vcenter/rest/vcenter/vm/$moref/power" -Method Get -Headers $session).value
Invoke-WebRequest -Uri "https://$vcenter/rest/vcenter/vm/$moref/power/stop" -Method Post -Headers $session
Invoke-WebRequest -Uri "https://$vcenter/rest/vcenter/vm/$moref/power/start" -Method Post -Headers $session
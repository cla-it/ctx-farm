provider "vsphere" {
  user           = "${var.vsphere_user}"
  password       = "${var.vsphere_password}"
  vsphere_server = "${var.vsphere_server}"

  # if you have a self-signed cert
  allow_unverified_ssl = true
}
data "vsphere_datacenter" "dc" {
  name = "dc01"
}
data "vsphere_host" "host" {
  name          = "esx-swanee.cl-grp.local"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
}
data "vsphere_host" "host" {
  name          = "esx-skyhook.cl-grp.local"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
} 
data "vsphere_host" "host" {
  name          = "esx-sunset.cl-grp.local"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"

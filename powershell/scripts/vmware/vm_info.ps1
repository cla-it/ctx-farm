#Gathering VM settings

Write-Host "Gathering VM statistics"

$Report = @()

Get-VM | Sort Name -Descending | %

 

$report = @()

 

foreach($vm in Get-View -ViewType Virtualmachine){

    $vms = "" | Select-Object VMName, Hostname, IPAddress, OS, Boottime, VMState, TotalCPU, CPUreservation, TotalMemoryGB, MemoryUsage, MemoryReservation, TotalNics, ToolsStatus, ToolsVersion, HardwareVersion, TimeSync, CBT, Portgroup, VMHost, ProvisionedSpaceGB, UsedSpaceGB, Datastore, Notes, FaultTolerance

    $vms.VMName = $vm.Name

    $vms.VMState = $vm.summary.runtime.powerState

    $vms.Boottime = $vm.Runtime.BootTime

    $vms.TotalNics = $vm.summary.config.numEthernetCards

    $vms.Portgroup = Get-View -Id $vm.Network -Property Name | select -ExpandProperty Name

    $vms.OS = $vm.Config.GuestFullName

    $vms.Hostname = $vm.guest.hostname

    $vms.IPAddress = $vm.guest.ipAddress

    $vms.VMHost = Get-View -Id $vm.Runtime.Host -property Name | select -ExpandProperty Name

    $vms.ProvisionedSpaceGB = [math]::Round($vm.Summary.Storage.UnCommitted/1GB,2)

    $vms.UsedSpaceGB = [math]::Round($vm.Summary.Storage.Committed/1GB,2)

    $vms.MemoryReservation = $vm.resourceconfig.memoryallocation.reservation

    $vms.CPUreservation = $vm.resourceconfig.cpuallocation.reservation       

    $vms.TotalCPU = $vm.summary.config.numcpu

    $vms.TotalMemoryGB = $vm.summary.config.memorysizem

    $vms.MemoryUsage = $vm.summary.quickStats.guestMemoryUsage   

    $vms.ToolsStatus = $vm.guest.toolsstatus

    $vms.ToolsVersion = $vm.config.tools.toolsversion

    $vms.TimeSync = $vm.Config.Tools.SyncTimeWithHost

    $vms.HardwareVersion = $vm.config.Version

    $vms.MemoryReservation = $vm.resourceconfig.memoryallocation.reservation

    $vms.Datastore = $vm.Config.DatastoreUrl[0].Name

    $vms.CBT = $vm.Config.ChangeTrackingEnabled 

    $vms.Datastore = $vm.Config.DatastoreUrl[0].Name

    $vms.Notes = $vm.Config.Annotation

    $vms.FaultTolerance = $vm.Runtime.FaultToleranceState

    $vms.SnapshotName = &{$script:snaps = Get-Snapshot -VM $vm.Name; $script:snaps.Name -join ','}

    $vms.SnapshotDate = $script:snaps.Created -join ','

    $vms.SnapshotSizeGB = $script:snaps.SizeGB -join ','

    $vms.Tags = (Get-TagAssignment -Entity $vm.Name).Tag.Name -join ','

    $vms.NB_last_backup =  Get-VM -Name $vm.Name | select -ExpandProperty Customfields | where{$_.Key -eq 'Last Backup'} | select -ExpandProperty Value

    $Report += $vms

}

 

#Output

if ($GridView -eq "yes") {

$report | Out-GridView }

 

if ($CreateCSV -eq "yes") {

$report | Export-Csv $FileCSV -NoTypeInformation -Path /home/gschifitto/Download/vmware.csv }
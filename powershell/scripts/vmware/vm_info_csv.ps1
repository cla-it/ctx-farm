Get-VM | %{

    $Summary = "" | Select Name, IP, NumCPU, MemoryGB, HDSize, `

       GuestOS, Datacenter, Folder, Id

    $Summary.Id = $_.Id

    $Summary.Name = $_.Name

    $Summary.IP = [String] $_.Guest.IPAddress

    $Summary.NumCPU = $_.NumCPU

    $Summary.MemoryGB = $_.MemoryGB

    $HD = @($_ | Get-HardDisk)

    $SizeMB = $HD[0].CapacityKB / 1024

    $Summary.HDSize = [Math]::Round($SizeMB, 0)

    $Summary.GuestOS = $_.Guest.OSFullName

    $Summary.Datacenter = (Get-Datacenter -VM $_).Name

    $Summary.Folder = $_.Folder.Name

    $Summary

} | Export-Csv  /tmp/cla-vms-repo.csv
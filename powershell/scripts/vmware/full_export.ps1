$table = @()

 

ForEach($VM in (Get-VM)){

    $Snapshots = $VM | Get-Snapshot

    $Report = "" | Select-Object VMname,vmCreatedByUser,vmCreatedDate,ESXname,ClusterName,MemoryGB,vCPUcount,VMXname,VmdkSizeGB,DatastoreName,SnapshotCount,Owner

    $Report.VMName = $VM.name

    $Report.vmCreatedByUser = Get-VIEvent $VM | sort createdTime | select -first 1 | select UserName | % {$_.UserName.split("\")[1]}

    $Report.vmCreatedDate = Get-VIEvent $VM | sort createdTime | select -first 1 | select CreatedTime

    $Report.ESXname = $VM.VMHost

    $Report.ClusterName = ($VM | Get-Cluster).Name

    $Report.MemoryGB = $VM.MemoryMB/1024

    $Report.vCPUcount = $VM.NumCpu

    $Report.VMXname = $VM.ExtensionData.Config.Files.VmPathName.Split("/")[1]

    $Report.VmdkSizeGB = $VM.UsedSpaceGB

    $Report.DatastoreName = $VM.ExtensionDataConfig.DatastoreUrl

    $Report.SnapshotCount = ($VM | Get-Snapshot).Count

    $Report.Owner = $VM | Get-VIPermission | Where-Object {$_.Role -like "xxxxxx*"} | select Principal | % {$_.Principal.split("\")[1]}

    $table += $Report

}

 

$table | Export-Csv report.csv -NoTypeInformation -UseCulture

$vmname = Read-Host -Prompt "Insert VM Name"
$guestuser = Read-Host -Prompt "Inster Username"
$guestpwd = Read-Host -Prompt "Insert Password"
$ipaddress = Read-Host -Prompt "Ipaddress"
$netmask = Read-Host -Prompt "Insert Netmask"
$gateway = Read-Host -Prompt "Insert Default Gateway"
$dns = Read-Host -Prompt "Insert DNS Primary"
#
# Install Tools VMware
#Invoke-VMScript -VM $vmwareVMName -GuestUser $guestuser -GuestPassword $guestpwd -ScriptText {'D:\setup64.exe /s /v "/qn reboot=r"'}
# Sysprep - After Script "Sysprep login via VMRemoteConsole and insert default Username & Password manually"
#Invoke-VMScript -VM $vmwareVMName -GuestUser $guestuser -GuestPassword $guestpwd -ScriptText "C:\Windows\System32\Sysprep\sysprep.exe /generalize /oobe /reboot"
#Set Ip Address Netmask & Gateway
Invoke-VMScript -VM $vmname -GuestUser $guestuser -GuestPassword $guestpwd {netsh interface ip set address "Ethernet0" static $ipaddress $netmask $gateway}
#Set DNS Primary
Invoke-VMScript -VM $vmname -GuestUser $guestuser -GuestPassword $guestpwd -ScriptType Powershell -ScriptText {netsh interface ip set dnsservers name="Ethernet0" static $dns primary}
#Disable Windows Firewall
Invoke-VMScript -VM $vmname -GuestUser $guestuser -GuestPassword $guestpwd -ScriptType Powershell -ScriptText {Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False}
#Enable RDP Session 
Invoke-VMScript -VM $vmname -GuestUser $guestuser -GuestPassword $guestpwd -ScriptText "netsh advfirewall firewall set rule group='Remote Desktop' new enable=yes"
Invoke-VMScript -vm $vmname -GuestUser $guestuser -GuestPassword $guestpwd -ScriptText "reg add 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' /f /v UserAuthentication /t REG_DWORD /d 0"
Invoke-VMScript -vm $vmname -GuestUser $guestuser -GuestPassword $guestpwd -ScriptText "reg add 'hklm\system\currentcontrolset\control\terminal server' /f /v fDenyTSConnections /t REG_DWORD /d 0"
#Change Hostname
Invoke-VMScript -VM $vmname -GuestUser $guestuser -GuestPassword $guestpwd -ScriptType Powershell  -ScriptText {"rename-computer -newname $vmwareVMName"}
#Reboot System
Invoke-VMScript -VM $vmname -GuestUser $guestuser -GuestPassword $guestpwd -ScriptType Powershell  -ScriptText {"restart-computer"}
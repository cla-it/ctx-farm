#Clone New-Template

    #Windows 10 64 Bit
    $uname = "ubuntusrv1604x64"
    $umnt = "$aireMount/tpl/ubuntusrv1604x64"
    $utpl = "$aireMount/tpl/ubuntusrv1604x64@tpl"
    $uvmx = "ubuntusrv1604x64.vmx"

$aireNfsIp = "10.250.251.71"
$team = Read-Host "Select RP Team (rp-elena,rp-access,rp-websplant,rp-splant,rp-arco,rp-devops)"
#$tpl = Read-Host "Select OS"
#$vmwareAuth = Get-Credential
$vmwareVMName = Read-Host "Name new Virtual Machine"
#$vmwareVMHost1 = "esx-sunset.cl-grp.local"
#$vmwareVMHost2 = "esx-swanee.cl-grp.local"
$vmwareVMHost3 = "esx-skyhook.cl-grp.local"



#Mount VMware NFS Datastore
Get-VMHost -Name $vmwareVMHost3 | New-Datastore -Nfs -NfsHost $aireNfsIp -Path "/sp0/$team/$vmwareVMName" -Name "$vmwareVMName" -Confirm:$false
#Register New Clone
New-VM -VMHost $vmwareVMHost3 -Name $vmwareVMName -VMFilePath "[$vmwareVMname] $uname/$uvmx" -ResourcePool (Get-VMHost -name $vmwareVMHost3 | Get-ResourcePool -Name "$team") -Confirm:$false

#Change VMnet
Get-VM -Name $vmwareVMName | Get-NetworkAdapter | Set-NetworkAdapter -Portgroup "dvlan21.vms0" -Confirm:$false

#Start VM & Answer Question
try
{
    try
    {
        Start-VM -VM $vmwareVMName -ErrorAction Stop -ErrorVariable custErr
    }
    catch [System.Management.Automation.ActionPreferenceStopException]
    {
        throw $_.Exception
    }
}
catch [VMware.VimAutomation.ViCore.Types.V1.ErrorHandling.VMBlockedByQuestionException]
{
   Get-VMQuestion -VM $vmwareVMName | Set-VMQuestion –Option button.uuid.copiedTheVM -Confirm:$false
}
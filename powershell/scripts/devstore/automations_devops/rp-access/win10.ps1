#AIRE-VMWARE TEMPLATE
    #Windows Server 2012r2 64 Bit
    #uname = "winsrv2k12r2x64"
    #$umnt = "$aireMount/tpl/winsrv2k12r2x64"
    #$utpl = "$aireMount/tpl/winsrv2k12r2x64@tpl"
    #$uvmx = "winsrv2k12r2x64.vmx"
    #
    #Windows Server 2016 64 Bit
    #$uname = "winsrv2k16x64"
    #$umnt = "$aireMount/tpl/winsrv2k16x64"
    #$utpl = "$aireMount/tpl/winsrv2k16x64@tpl"
    #$uvmx = "winsrv2k16x64.vmx"
    #
    #Windows 10 64 Bit
    $uname = "win10x64"
    $umnt = "$aireMount/tpl/win10x64"
    $utpl = "$aireMount/tpl/win10x64@tpl"
    $uvmx = "win10x64.vmx"
    #
    #Windows 7 64 Bit
    #$uname = "win7x64"
    #$umnt = "$aireMount/tpl/win7x64"
    #$utpl = "$aireMount/tpl/win7x64@tpl"
    #$uvmx = "win7x64.vmx"
    #
    #Windows 7 32 Bit
    #$uname = "win7x32"
    #$umnt = "$aireMount/tpl/win7x32"
    #$utpl = "$aireMount/tpl/win7x32@tpl"
    #$uvmx = "win7x32.vmx"
#
#
$aireNfsIp = "10.250.251.71"
$team = Read-Host "Select RP Team (rp-elena,rp-access,rp-websplant,rp-splant,rp-arco,rp-devops)"
#$tpl = Read-Host "Select OS"
#$vmwareAuth = Get-Credential
$vmwareVMName = Read-Host "Name new Virtual Machine"
$vmwareVMHost1 = "esx-sunset.cl-grp.local"
$vmwareVMHost2 = "esx-swanee.cl-grp.local"


#Mount VMware NFS Datastore
Get-VMHost -Name $vmwareVMHost1,$vmwareVMHost2 | New-Datastore -Nfs -NfsHost $aireNfsIp -Path "/sp0/$team/$vmwareVMName" -Name "$vmwareVMName" -Confirm:$false
#Register New Clone
New-VM -VMHost $vmwareVMHost1 -Name $vmwareVMName -VMFilePath "[$vmwareVMname] $uname/$uvmx" -ResourcePool (Get-VMHost -name $vmwareVMHost1 | Get-ResourcePool -Name "$team") -Confirm:$false

#Change VMnet
Get-VM -Name $vmwareVMName | Get-NetworkAdapter | Set-NetworkAdapter -Portgroup "dvlan102.vms0" -Confirm:$false
#Set-VmnxNet3
Get-VM -Name $vmwareVMName | Get-NetworkAdapter | Set-NetworkAdapter -type "vmxnet3" -Confirm:$false
#Start VM & Answer Question
try
{
    try
    {
        Start-VM -VM $vmwareVMName -ErrorAction Stop -ErrorVariable custErr
    }
    catch [System.Management.Automation.ActionPreferenceStopException]
    {
        throw $_.Exception
    }
}
catch [VMware.VimAutomation.ViCore.Types.V1.ErrorHandling.VMBlockedByQuestionException]
{
   Get-VMQuestion -VM $vmwareVMName | Set-VMQuestion –Option button.uuid.copiedTheVM -Confirm:$false
}
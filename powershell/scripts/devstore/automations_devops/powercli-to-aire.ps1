#AIRE-VMWARE TEMPLATE
    #Windows Server 2012r2 64 Bit
    $winsrv2k12r2x64name = "winsrv2k12r2x64"
    $winsrv2k12r2x64mnt = "$aireMount/tpl/winsrv2k12r2x64"
    $winsrv2k12r2x64tpl = "$aireMount/tpl/winsrv2k12r2x64@tpl"
    $winsrv2k12r2x64vmx = "winsrv2k12r2x64.vmx"
    #
    #Windows Server 2016 64 Bit
    $winsrv2k16x64name = "winsrv2k16x64"
    $winsrv2k16x64mnt = "$aireMount/tpl/winsrv2k16x64"
    $winsrv2k16x64tpl = "$aireMount/tpl/winsrv2k16x64@tpl"
    $winsrv2k16x64vmx = "winsrv2k16x64.vmx"
    #
    #Windows 10 64 Bit
    $win10name = "win10x64"
    $win10mnt = "$aireMount/tpl/win10x64"
    $win10tpl = "$aireMount/tpl/win10x64@tpl"
    $win10vmx = "win10x64.vmx"
    #
    #Windows 7 64 Bit
    $win7x64name = "win7x64"
    $win7x64mnt = "$aireMount/tpl/win7x64"
    $win7x64tpl = "$aireMount/tpl/win7x64@tpl"
    $win7x64vmx = "win7x64.vmx"
    #
    #Windows 7 32 Bit
    $win7x32name = "win7x32"
    $win7x32mnt = "$aireMount/tpl/win7x32"
    $win7x32tpl = "$aireMount/tpl/win7x32@tpl"
    $win7x32vmx = "win7x32.vmx"
#
#
#$aireAuth = 
$aireMgmtHost= Read-Host "Insert Aire Management"
$aireNfsHost = Read-Host "Insert Aire NFS Mount IP-Address"
$aireMount = Read-Host "Insert Dataset"
#
#DEV RESURCE TEAM
$team = Read-Host "Select RP Team (rp-elena,rp-access,rp-websplant,rp-splant,rp-arco)"

    $elenaName = "elena"
    $elenaRpName = "rp-elena"
    $elenaMnt = "sp0/rp-elena"

    $accessName = "access"
    $accessRpName = "rp-access"
    $accessMnt = "sp0/sp-access"
    
    $websplantName = "websplant"
    $websplantRpName = "rp-websplant"
    $websplant = "sp0/websplant"
    
    $splentName = "splant"
    $splantRpName = "rp-splant"
    $splantMnt = "sp0/splant"
    
    $arcoName = "arco"
    $arcoRpName = "rp-arco" 
    $arcoMnt= "sp0/arco"


$tpl = Read-Host "Select OS"
#VMware 
$vmwareAuth = Get-Credential
$vmwareVMName = Read-Host "Name new Virtual Machine"
$vmwareVMHost1 = "esx-sunset.cl-grp.local"
$vmwareVMHost2 = "esx-swanee.cl-grp.local"



#Mount VMware NFS Datastore
Get-VMHost -Name $vmwareVMHost1,$vmwareVMHost2 | New-Datastore -Nfs -NfsHost $aireNfsHost -Path "/$airemount/$vmwareVMName" -Name "$vmwareVMName" -Confirm:$false

#Register New Clone
New-VM -VMHost $vmwareVMHost1 -Name $vmwareVMName -VMFilePath "[$vmwareVMname] $win10name/$win10vmx" -ResourcePool (Get-VMHost -name $vmwareVMHost1 | Get-ResourcePool -Name $team) -Confirm:$false

#Change Network
Get-VM -Name "websplant-wrk-dev0" | Get-NetworkAdapter | Set-NetworkAdapter -Portgroup $ -Confirm:$false

#Start VM & Answer Question

try
{
    try
    {
        Start-VM -VM $vmwareVMName -ErrorAction Stop -ErrorVariable custErr
    }
    catch [System.Management.Automation.ActionPreferenceStopException]
    {
        throw $_.Exception
    }
}
catch [VMware.VimAutomation.ViCore.Types.V1.ErrorHandling.VMBlockedByQuestionException]
{
   Get-VMQuestion -VM $vmwareVMName | Set-VMQuestion –Option button.uuid.copiedTheVM -Confirm:$false
}


$guestuser = Read-Host "Insert Guest User"
$guestpwd  = Read-Host "Insert Guest Password"
$ipaddress = Read-Host -Prompt "Ipaddress"
$netmask = Read-Host -Prompt "Insert Netmask"
$gateway = Read-Host -Prompt "Insert Default Gateway"
$dns1 = Read-Host -Prompt "Insert DNS Primary"

    #Mount VM-Tools
    Mount-Tools -VM $vmwareVMName
    #Install Tools
    Invoke-VMScript -VM $vmwareVMName -GuestUser $guestuser -GuestPassword $guestpwd -ScriptText {'D:\setup64.exe /s /v "/qn reboot=r"'}
    #Run SysPrep
    Invoke-VMScript -VM $vmwareVMName -GuestUser $guestuser -GuestPassword $guestpwd -ScriptText "C:\Windows\System32\Sysprep\sysprep.exe /generalize /oobe /reboot"
    #Set IP address
    Invoke-VMScript -VM $vmwareVMName -GuestUser $guestuser -GuestPassword $guestpwd -ScriptType Powershell -ScriptText {netsh interface ip set address "Ethernet0" static $ipaddress $netmask $gateway}
    #Set DNS
    Invoke-VMScript -VM $vmwareVMName -GuestUser $guestuser -GuestPassword $guestpwd -ScriptType Powershell  -ScriptText {netsh interface ip set dnsservers name="Ethernet0" static $dns1 primary}
    #Disable Firewall
    Invoke-VMScript -VM $vmwareVMName -GuestUser $guestuser -GuestPassword $guestpwd -ScriptType Powershell  -ScriptText {Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False}
    #Change Hostname
    Invoke-VMScript -VM $vmwareVMName -GuestUser $guestuser -GuestPassword $guestpwd -ScriptType Powershell  -ScriptText {"rename-computer -newname $vmwareVMName"}
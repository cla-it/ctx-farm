#Clone New-Template

    #Windows Server 2012r2 64 Bit
    $winsrv2k12r2x64name = "winsrv2k12r2x64"
    $winsrv2k12r2x64mnt = "$aireMount/tpl/winsrv2k12r2x64"
    $winsrv2k12r2x64tpl = "$aireMount/tpl/winsrv2k12r2x64@tpl"
    $winsrv2k12r2x64vmx = "winsrv2k12r2x64.vmx"

$aireNfsIp = "10.250.251.71"
$team = Read-Host "Select RP Team (rp-elena,rp-access,rp-websplant,rp-splant,rp-arco)"
#$tpl = Read-Host "Select OS"
#$vmwareAuth = Get-Credential
$vmwareVMName = Read-Host "Name new Virtual Machine"
$vmwareVMHost1 = "esx-sunset.cl-grp.local"
$vmwareVMHost2 = "esx-swanee.cl-grp.local"



#Mount VMware NFS Datastore
Get-VMHost -Name $vmwareVMHost1,$vmwareVMHost2 | New-Datastore -Nfs -NfsHost $aireNfsIp -Path "/sp0/$team/$vmwareVMName" -Name "$vmwareVMName" -Confirm:$false
#Register New Clone
New-VM -VMHost $vmwareVMHost1 -Name $vmwareVMName -VMFilePath "[$vmwareVMname] $winsrv2k12r2x64name/$winsrv2k12r2x64vmx" -ResourcePool (Get-VMHost -name $vmwareVMHost1 | Get-ResourcePool -Name "$team") -Confirm:$false

#Change VMnet
Get-VM -Name $vmwareVMName | Get-NetworkAdapter | Set-NetworkAdapter -Portgroup "dvlan101.vms0" -Confirm:$false

#Start VM & Answer Question
try
{
    try
    {
        Start-VM -VM $vmwareVMName -ErrorAction Stop -ErrorVariable custErr
    }
    catch [System.Management.Automation.ActionPreferenceStopException]
    {
        throw $_.Exception
    }
}
catch [VMware.VimAutomation.ViCore.Types.V1.ErrorHandling.VMBlockedByQuestionException]
{
   Get-VMQuestion -VM $vmwareVMName | Set-VMQuestion –Option button.uuid.copiedTheVM -Confirm:$false
}

#2016 by Will P
#Enables RDP in a Windows 2012 VM
#get credentials with admin access
$guestcred = Get-Credential
#Gather VM's
$vms = Get-Datacenter get-cluster "Cluster Name Here" | get-vm
#Loop through VM's, running the commands to enable RDP and modify firewall rules
foreach($vm in $vms){
Invoke-VMScript -VM $vm -GuestCredential $guestcred -ScriptText "netsh advfirewall firewall set rule group='Remote Desktop' new enable=yes"
Invoke-VMScript -vm $vm -GuestCredential $guestcred -ScriptText "reg add 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' /f /v UserAuthentication /t REG_DWORD /d 0"
Invoke-VMScript -vm $vm -GuestCredential $guestcred -ScriptText "reg add 'hklm\system\currentcontrolset\control\terminal server' /f /v fDenyTSConnections /t REG_DWORD /d 0"
}
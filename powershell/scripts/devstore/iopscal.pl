#!/usr/bin/perl
# Purpose: estimate Solaris IOPS rate, and %read/%write
$VERSION="1.1, Feb,2013";

# set the next value to be greater than 0.
# Turn off by setting to 0

# Help screen
if ( $ARGV[0] eq "-h") {
        $PRGM=$0;
        system("tput clear");
        print("Program name:  $PRGM, Version $VERSION \n\n");
        print("Purpose: Calculate Total, Avg, Max IOPS from iostat raw output in Solaris.\n\n");
        print("Usage:  $PRGM [iostat file]\n\n\n");
        print("Example 1: Input from file.\n");
        print("\n\tiostat -xncz 5 > iostat.out\n");
        print("Note: Be sure there is a empty line at the end of the iostat output file.\n");
        print("\t$PRGM iostat.out \n\n");
        print("Example 2: Input from stdin.\n");
        print("\n\tiostat -xncz 5 30 \| $PRGM\n\n");
        print("Note: Be sure there is more than one iostat interval in the sample.\n");
        print("This program skips the first interval (historic data).\n\n");
        exit(0);
} # end if


# Determine if input is stdin or file
if (@ARGV == 0) {
        #No file specified, assume stdin
        $INFILE="<&0";
        $|=1; # Don't buffer input
} else{
        $INFILE=$ARGV[0];
} # end if

# Open file or stdin
unless (open(INPUT, $INFILE)) { die ("File open error\n");}

# Debug
# unless (open(INPUT, "iostat.out")) { die ("File open error\n");}


# Read the iostat data for each interval
# 1. Data for each interval starts after line "extended device statistics."
# 2. Read interval data. Output interval stats. Accumulate overall stats.
# 3. Interval data ends with "cpu" or eof.

# Initialisation ---
$max_rps=0;
$max_wps=0;
$max_krps=0;
$max_kwps=0;
$max_mrps=0;
$max_mwps=0;
$max_iops=0;
$max_avst=0;
$max_wait=0;
$max_busy=0;

# Print header for output
$~="HEADER";
write;
$~="LINE";
write;

# Switch to column format for output
$~="COLUMNS";


# Skip first iostat interval because it is historic data
&next_interval;

# read iostat data for each interval...the $read_next subroutine detects eof
while ( 1 ) {

        # Skip to first line with data in next interval
        &next_interval;

        # Zero counters for this interval
        &zerocounters;

        # Start reading the data, the &read_next checks for eof
        while ( &read_next ) {
                
                $SIG{INT} = break;

                # Check for end of interval (CDROM or empty line)
                if ($input =~ /cpu/ || $input =~ /tty/ || $input eq "") {last;}

                # Parse the line for I/O data
                @cols = split(/\s+/,$input);
                $count++;
                $sum_rps+=$cols[1];
                $sum_wps+=$cols[2];
                $sum_krps+=$cols[3];
                $sum_kwps+=$cols[4];
                if ( $cols[1]+$cols[2] > 0 ) {
                        $sum_iops+=$cols[1]+$cols[2];
                        $iops_count++;
                } #end if
                if ( $cols[8] > 0 ) {
                        $sum_avst+=$cols[8];
                        $avstcount++;                   
                } #end if
                $sum_wait+=$cols[9];
                if ( $cols[10] > 0 ) {
                        $sum_busy+=$cols[10];
                        $bsycount++;
                } #end if

        } # end while

        # Summarize the I/O data for this interval
        &summarize;
        &maximum;

}# end while

close(INPUT);



##############################################################
#  Subroutines
##############################################################

# Calculate grand totals
sub grandtotals() {

        # Calculate the overall statistics
        # Reuse interval variables so we can use the write function

        # Set print format
        $~="COLUMNS";

        if ($interval >0) {

                $sum_rps = $rps_overall/$interval;
                $sum_wps  = $wps_overall/$interval;
                $sum_krps = $krps_overall/$interval;
                $sum_kwps = $kwps_overall/$interval;
                $sum_avst = $avst_overall/$interval;
                $sum_wait = $wait_overall/$interval;
                $sum_busy = $busy_overall/$interval;
                $sum      = $rps_overall + $wps_overall;
                $sum_mrps = $sum_krps/1024;
                $sum_mwps = $sum_kwps/1024;

                if ( $sum >0 ) {
                        $pct_read=100*($rps_overall)/$sum;
                        $pct_write=100 - $pct_read;
                } else {
                        $pct_read="";
                        $pct_write="";
                }

                if ($iops_overall_cnt >0 ){
                        $iops=$iops_overall/$iops_overall_cnt;
                        $k_iops=$iops/1000;
                } else {
                        $iops="";
                } # end if

                # Save the interval count in "count"
                $count=$interval;
                $interval="Avg";

                # Print horizontal line
                $~="LINE1";
                write;

                # Print the overall averages
                $interval="Avg";
                $~="COLUMNS";
                write;

                # Print horizontal line
                $~="LINE";
                write;

                # Print the maximum values
                $interval="Max";
                $~="MAX";
                write;

        } #end if

        # Print horizontal line
        $~="LINE";
        write;

} # end grandtotals


# Summarizes the interval statistics
sub summarize() {

        if ( $count < 1  ) {
                print ("Input error: Disk count equals zero. Exiting\n");
                exit;
        } # end if

        # Interval statistics

        $sum= $sum_rps + $sum_wps;
        if ( $sum > 0 ) {
                $pct_read= 100*($sum_rps)/$sum;
                $pct_write=100- $pct_read;
        } else {
                $pct_read=" ";
                $pct_write=" ";
        }

        if ($iops_count == 0) {
                $iops=" ";
        } else {
#               $iops=$sum_iops/$iops_count;
                $iops=$sum_iops;
        } # end if


        # Sum statistics for global sample
        $interval++;
        $rps_overall+=$sum_rps;
        $wps_overall+=$sum_wps;
        $krps_overall+=$sum_krps;
        $kwps_overall+=$sum_kwps;
        $sum_mrps = $sum_krps/1024;
        $sum_mwps = $sum_kwps/1024;
        if ($iops_count >0) {
                $iops_overall+= $iops;
                $iops_overall_cnt++;
        } # end if
        $k_iops=$iops/1000;
        if ( $avstcount == 0 ) {
                $avstcount=1;
        } #end if
        $sum_avst=$sum_avst/$avstcount;
        $sum_wait=$sum_wait/$count;
        if ( $bsycount == 0 ) {
                $bsycount=1;
        } #end if
        $sum_busy=$sum_busy/$bsycount;
        $avst_overall+=$sum_avst;
        $wait_overall+=$sum_wait;
        $busy_overall+=$sum_busy;
        # Print out interval summary
        write;

} #end summarize

# Find maximum value
sub maximum() {

        # Interval statistics
        #--Max calculation
        if ( $sum_rps > $max_rps ) {
                        $max_rps = $sum_rps
                }
        if ( $sum_wps > $max_wps ) {
                        $max_wps = $sum_wps
                }
        if ( $sum_krps > $max_krps ) {
                        $max_krps = $sum_krps
                }
        if ( $sum_kwps > $max_kwps ) {
                        $max_kwps = $sum_kwps
                }
        if ( $sum_iops > $max_iops ) {
                $max_iops = $sum_iops
           }
        if ( $sum_avst > $max_avst ) {
                $max_avst = $sum_avst
           }
        if ( $sum_wait > $max_wait ) {
                $max_wait = $sum_wait
           }
        if ( $sum_busy > $max_busy ) {
                $max_busy = $sum_busy
           }
        $kmax_rps=$max_rps/1000;
        $kmax_wps=$max_wps/1000;
        $kmax_iops=$max_iops/1000;
        $max_mrps=$max_krps/1024;
        $max_mwps=$max_kwps/1024;
} #end maximum


# Zero out interval counters
sub zerocounters() {
        $count=0;
        $avstcount=0;
        $bsycount=0;
        $sum_wps=0;
        $sum_krps=0;
        $sum_kwps=0;
        $sum_mrps=0;
        $sum_mwps=0;
        $sum_iops=0;
        $sum_avst=0;
        $sum_wait=0;
        $sum_busy=0;
        $iops_count=0;
        $sum_rps=0;
        $k_iops=0;
} # end zerocounters


# Increment line to next iostat interval
sub next_interval {


        #Increment to beginning of next iostat interval
        do {
                &read_next;
        } until ($input =~ /extended/);


} # end next_interval


# Read next line
sub read_next {

        $input=<INPUT> ;

        # check for end of file
        if ( eof ) {
                &summarize;
                &grandtotals;
                exit;
        } #end if

        chop($input);

} # end read_next



##############################################################
#  Print formats
##############################################################

format HEADER =
      |--------------TOTAL IOPS--------------------| |------KB_Read------| |-----KB_Write------| |------Device Status------|
Count        r/s         w/s        io/s   io/s(K)      kr/s                kw/s                  AvgSrvTm %Wait %DiskBusy
.

format COLUMNS =
@<<<@####.# (@##%) @####.# (@##%) @##### (@##.##K) @###### (@###.##MB/s) @###### (@###.#MB/s)  @####.#ms @###.#%  @###% 
$interval, $sum_rps, $pct_read, $sum_wps, $pct_write, $iops, $k_iops, $sum_krps, $sum_mrps, $sum_kwps, $sum_mwps, $sum_avst, $sum_wait, $sum_busy
.

format MAX =
@<<<@####.#(@#.##K)@####.#(@#.##K)@##### (@##.##K) @###### (@###.##MB/s) @###### (@###.#MB/s)  @####.#ms @###.#%  @###%
$interval, $max_rps, $kmax_rps, $max_wps, $kmax_wps, $max_iops, $kmax_iops, $max_krps, $max_mrps, $max_kwps, $max_mwps, $max_avst, $max_wait, $max_busy
.

format LINE =
----------------------------------------------------------------------------------------------------------------------------
.

format LINE1 =
-------Reads/s---------Writes/s---------IOPS-----------KBRead/s--------------KBWrite/s-------------asvc_t--%Wait--%DiskBusy-
.